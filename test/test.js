'use strict'

const { Worker } = require('worker_threads')
const expect = require('chai').expect;


/** keeps the resolve and reject info  */
const resolves = {};
const rejects = {};
/** keeps the Unique Id */
let globalMsgId = 0;
/** Keeps worker variable */
var worker;

/**
 * Wrapped the Message and send it with the promise
 * @param {hash} payload  payload to send.
 * @param {object} worker worker value.
 */
function sendMsg(payload, worker) {
    const msgId = globalMsgId++;
    const msg = {
        id: msgId
    };
    Object.assign(msg, payload);
    return new Promise(function(resolve, reject) {
        // save callbacks for later
        resolves[msgId] = resolve;
        rejects[msgId] = reject;

        worker.postMessage(msg);
    });
}

/**
 * Internal function that resolve the promise
 * @param {hash} msg received message. 
 */
function handleMsg(msg) {

    if (msg.action == "loaded") {

    } else if (msg.action == "ErrorMode") {
        // error condition
        const reject = rejects[msg.id];
        if (reject) {
            reject('error');
        }
    } else if (msg.message) {
        let mes = [];
        if (msg.string) {
            mes = msg.string.split(":");
        }
        if (mes[0] == "Error") {
            // error condition
            const reject = rejects[msg.id];
            if (reject) {
                if (mes[1]) {
                    reject(mes[1]);
                }
            }
        } else {
            const resolve = resolves[msg.id];
            if (resolve) {
                resolve(msg);
            }
        }
    } else {
        // error condition
        const reject = rejects[msg.id];
        if (reject) {
            if (msg.err) {
                reject(msg.err);
            } else {
                reject('Got nothing');
            }
        }
    }

    // purge used callbacks
    delete resolves[msg.id];
    delete rejects[msg.id];
}

/**
 * First test that start the worker
 */
function startWorker() {
    return new Promise(resolve => {
        worker = new Worker('./jsAgrum/lib/nodejs/libagrum.js');

        worker.on('message', handleMsg);
        worker.on('online', () => { resolve('loaded') });
    });
}

/** start tests */
describe('jsAgrum Basic Tests', () => {

    /** Workaround to avoid race condition between tests */
    beforeEach(function(done) {
        setTimeout(function() {
            // complete the async beforeEach
            done();
        }, 1000);

    });

    /** start worker */
    it('loading', async() => {
        const result = await startWorker();
        expect(result).to.equal('loaded');
    });

    /** create a new BN */
    it('creation', async() => {
        const result = await sendMsg({ action: "createBN", name: "toto" }, worker);
        expect(result.message).to.equal('createBN has been executed. (toto)');
    });

    /** add first node */
    it('add Node', async() => {
        const result = await sendMsg({
            action: "addNode",
            name: "N0",
            nbrmod: 2,
            posX: 10,
            posY: 10
        }, worker);
        expect(result.message).to.equal('addNode has been executed.');
    });

    /** add second node */
    it('add Node2', async() => {
        const result = await sendMsg({
            action: "addNode",
            name: "N1",
            nbrmod: 2,
            posX: 20,
            posY: 20
        }, worker);
        expect(result.message).to.equal('addNode has been executed.');
    });

    /** add 1 arc */
    it('add arc', async() => {
        const result = await sendMsg({
            action: "addArc",
            tail: 0,
            head: 1
        }, worker);
        expect(result.message).to.equal('addArc has been executed.');
    });

    /** check the BN content */
    it('to string', async() => {
        const result = await sendMsg({
            action: "toString"
        }, worker);
        expect(result.string).to.equal('BN{nodes: 2, arcs: 1, domainSize: 4, dim: 6}');
    });

    /** terminate the worker to end the test */
    it('terminated', () => {
        worker.terminate();
    });

});