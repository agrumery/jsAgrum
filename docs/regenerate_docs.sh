#!/bin/bash
# To uses in main folder when deploy exists

if hash jsdoc 2>/dev/null; then
    jsdoc -d ./deploy/docs/jusu/ -r ./apps/jusu/js/ ./apps/jusu/README.md
    jsdoc -d ./deploy/docs/jsAgrum/ -r ./jsAgrum/src/js ./README.md
else
    echo "You need to install jsdoc (https://github.com/jsdoc/jsdoc)"
fi
