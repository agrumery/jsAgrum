#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyAgrum as gum
import pydotplus as dot
import json
import tempfile
import os
from zipfile import ZipFile
import sys


def loadVBN(filename: str, ferr) -> gum.BayesNet:
  with tempfile.TemporaryDirectory() as tmpdirname:
    with ZipFile(filename, "r") as vbn:
      bn = None
      bnname = ""
      cwd = os.getcwd()
      for name in vbn.namelist():
        _, e = os.path.splitext(name)
        if e != "jusu":
          print(f"{filename} ({name}) :", end="", flush=True)
          vbn.extract(name, path=tmpdirname)
          os.chdir(tmpdirname)
          try:
            bn = gum.loadBN("./" + name)
            print(bn)
          except gum.FatalError as e:
            print(" error(s) found")
            print("*" * 50, file=ferr)
            print(name, file=ferr)
            print("*" * 50, file=ferr)
            print(e.what(), file=ferr)
            print("\n"*2, file=ferr)
          os.remove(name)
          os.chdir(cwd)
          bnname = name
          break


with open("loadVBN.log", "w") as ferr:
  for fn in sys.argv[1:]:
    loadVBN(fn, ferr)
