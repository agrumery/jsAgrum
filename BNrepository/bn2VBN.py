#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyAgrum as gum
import pydotplus as dot
import json
import tempfile
import os
from zipfile import ZipFile
import sys


def saveAsVBN(bn, name, ext):
  g = dot.graph_from_dot_data(bn.toDot())
  tab = json.loads(g.create(format='json'))
  for d in tab['_draw_']:
    if 'points' in d:
      maxx, maxy = d['points'][2]
  nods = []
  for nod in tab['objects']:
    n = nod['name']
    x, y = nod['pos'].split(",")
    nods.append(f'"{n}":{{"X":{x},"Y":{str(float(maxy)-float(y))}}}')

  with tempfile.TemporaryDirectory() as tmpdirname:
    bnfile = os.path.join(tmpdirname, name+"."+ext)
    gum.saveBN(bn, bnfile)

    jsonfile = os.path.join(tmpdirname, name+".json")
    with open(jsonfile, "w") as jf:
      print("{"+",".join(nods)+"}", file=jf)

    with ZipFile(name+"_"+ext+".vbn", "w") as myzip:
      myzip.write(bnfile, arcname=name+"."+ext)
      myzip.write(jsonfile, arcname=name + ".jusu")


exts = set(gum.availableBNExts().split("|"))

extension = False
extension_done = False
exp_ext = ""
for fn in sys.argv[1:]:
  if fn == "-e":
    if extension_done:
      print("Error : -e must be first arg.")
    extension = True
    continue
  if extension:
    if fn == "all":
      exp_ext = "all"
    else:
      if fn not in exts:
        print("Extension <" + fn + "> not in " + str(exts))
      else:
        print("Exported extension :" + fn)
        exp_ext = fn
    extension = False
    continue

  extension_done = True
  filename, ext = os.path.splitext(fn)
  print("processing "+fn, end="")
  ext = ext[1:]  # removing the dot
  if ext not in exts:
    print("-> extension <"+ext + "> not in " + str(exts))
    continue
  try:
    bn = gum.loadBN(fn)   
    bn.setProperty("name", filename)
    print(f"({bn}) :",end="",flush=True)   
  except gum.GumException as e :
    print("-> error")
    print(e.what())
    continue
  if exp_ext == "all":
    for exp_tmp in ['bif', 'bifxml', 'dsl', 'o3prm', 'net']:
      print(exp_tmp, end=" ")
      saveAsVBN(bn, filename, exp_tmp)
  else:
    saveAsVBN(bn, filename, ext if exp_ext == "" else exp_ext)
  print("-> OK")
