# jsAgrum
Mock-up using the aGrUM library transcoded into javascript with [Emscripten](https://emscripten.org) and providing some wrappers to be used in a browser.

## Installation

jsAgrum needs to be compiled with [Emscripten](https://emscripten.org). Please go to the [documentation page](https://emscripten.org/docs/getting_started/downloads.html) to download and install it.


```sh
$ cd jsAgrum
$ chmod +x build_libagrum_js.sh
$ ./build_libagrum_js.sh
$ make all
$ make deploy_local
```

Test the mock-up by running a web server at the root of jsAgrum, e.g.:

```sh
$ php -S localhost:8080 -t .
```

## emsdk
- git clone https://github.com/emscripten-core/emsdk.git
- cd emsdk

MAYBE (?)

- ./emsdk install 1.38.45
- ./emsdk activate 1.38.45

OR

- ./emsdk install latest-fastcomp

THEN

- source emsdk_env.YOURSHELL

## How it works, how to use ?

Emsk generates wasm code based on the C++ code and combines it with 2 JS files. (libagrum_before.js and libagrum_after.js) These files allow to add the necessary Javascript code around the transcoded one. It results 2 files libagrum.js and libagrum.wasm.
This generated code is run inside a worker in parallel of the main web page thread. The communication with the worker is only some asynchronous messages. The philosophy of the messages is to keep the message closes to the original C++ call. This way avoid to multiply the command and try to keep it to minimal.

libjsagrum_utils.js is a file containing some functions to help the user (ex: to load BN and VBN files). This files deploys the worker and wraps it with a promise model.
```js
createBN("test").then( ... ).catch( ... );
```
This line will send the message to the worker and wait the response to resolve the promise.

To manage VBN files, the library "jsZip" is needed. To save VBN files, the library "download" is needed. 
To add the library use these lines in the index.html, of a new app inside the repository.

```js
<script type="application/javascript" src="../requirements/jsZip/jszip.js" charset="utf8" async></script>
<script type="application/javascript" src="../requirements/jsZip/jszip-utils.js" charset="utf8" async></script>
<script type="application/javascript" src="../requirements/Download.js/download.js" charset="utf8" async></script>
<script type="application/javascript" src="../../jsAgrum/lib/libjsagrum_utils.js" charset="utf8"></script>
```

To develop a new application, it exists in "apps/" minimalDemo.


## Electron version (WIP)
It is possible to use jsAgrum in a standalone 64bits version app (linux, windows and MacOS). In order to generate it, you have to install some stuff.

```bash
sudo apt-get install nodejs npm 
npm install electron --save-dev 
npm install electron-packager --save-dev
npm install electron-packager -g
npm install electron-debug
```
And after to generate it

```bash
make deploy_electron
cd deploy_electron
. generate
```

## jsAgrum test (WIP)
Unit test using Mocka

```bash
npm install mocha -g
npm install chai -g
```
generate jsAgrum for nodeJs
```bash
make compile_node_version
```
start the test
```bash
mocha
```

## Todos

 - So many things !

License
----

???
