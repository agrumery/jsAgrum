/**
 * Worker
 */

/** @module Worker Utilities */

// Global variables
/** keeps the transcoded object */
var BN = undefined;

/**
 * handles the message from the main thread
 */
onmessage = function(evt) {
    var data;
    if (NODE_VERSION) {
        data = evt;
    } else {
        data = evt.data;
    }

    switch (data.action) {
        case "createBN":
            if (BN !== undefined) {
                BN.delete();
                BN = undefined;
            }
            BN = new Module.BayesNetDouble(data.name);
            workerSendMsg({ id: data.id, action: data.action, message: data.action + " has been executed. (" + data.name + ")" });
            break;
        case "addNode":
            workerSendMsg({ id: data.id, action: data.action, name: data.name, nodeId: BN.AddNode(data.name, data.nbrmod, data.posX, data.posY), message: data.action + " has been executed." });
            break;
        case "removeNode":
            workerSendMsg({ id: data.id, action: data.action, string: BN.RemoveNode(data.nodeId), message: data.action + " has been executed." });
            break;
        case "changeNodeName":
            workerSendMsg({ id: data.id, action: data.action, string: BN.ChangeNodeName(data.nodeId, data.name), message: data.action + " has been executed." });
            break;
        case "changeLabelsName":
            workerSendMsg({ id: data.id, action: data.action, string: BN.ChangeLabelsName(data.nodeId, data.jsontxt), message: data.action + " has been executed." });
            break;
        case "addArc":
            workerSendMsg({ id: data.id, action: data.action, string: BN.AddArc(data.tail, data.head), message: data.action + " has been executed." });
            break;
        case "removeArc":
            workerSendMsg({ id: data.id, action: data.action, string: BN.RemoveArc(data.tail, data.head), message: data.action + " has been executed." });
            break;
        case "toString":
            workerSendMsg({ id: data.id, action: data.action, string: BN.ToString(), message: data.action + " has been executed." });
            break;
        case "generateCpts":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GeneraCTPs(), message: data.action + " has been executed." });
            break;
        case "cpt":
            workerSendMsg({ id: data.id, action: data.action, string: BN.Cpt(data.nodeId), message: data.action + " has been executed." });
            break;
        case "toDot":
            workerSendMsg({ id: data.id, action: data.action, string: BN.ToDot(), message: data.action + " has been executed." });
            break;
        case "getNodesList":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetNodesList(), message: data.action + " has been executed." });
            break;
        case "getArcsList":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetArcsList(), message: data.action + " has been executed." });
            break;
        case "addCpt":
            workerSendMsg({ id: data.id, action: data.action, string: BN.AddCpt(data.nodeId, data.jsontxt), message: data.action + " has been executed." });
            break;
        case "moveNode":
            workerSendMsg({ id: data.id, action: data.action, string: BN.MoveNode(data.nodeId, data.x, data.y), message: data.action + " has been executed." });
            break;
        case "getPosNode":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetPosNode(data.nodeId), message: data.action + " has been executed." });
            break;
        case "getAllPosNode":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetAllPosNode(), message: data.action + " has been executed." });
            break;
        case "loadBN":
            FS.writeFile(data.name, data.contents, { encoding: "utf8", flags: "w" });
            let name = "./" + data.name;
            workerSendMsg({
                id: data.id,
                action: data.action,
                string: BN.LoadBN(name, data.type),
                posfile: BN.LoadBNPos(data.posfile),
                message: data.action + " has been executed."
            });
            break;
        case "saveBN":
            BN.SaveBN("tmp.tmp");
            BN.SaveBNPos("tmp.json");
            workerSendMsg({
                id: data.id,
                action: data.action,
                filename: BN.GetName(),
                contents: FS.readFile("tmp.tmp", { encoding: "utf8" }),
                posfile: FS.readFile("tmp.json", { encoding: "utf8" }),
                message: data.filename + " has been loaded."
            });
            break;
        case "posterior":
            workerSendMsg({ id: data.id, action: data.action, string: BN.Posterior(data.nodeId), message: data.action + " has been executed." });
            break;
        case "addEvidence":
            workerSendMsg({ id: data.id, action: data.action, string: BN.AddEvidence(data.nodeId, data.evidence), message: data.action + " has been executed." });
            break;
        case "resetEvidenceEngine":
            workerSendMsg({ id: data.id, action: data.action, string: BN.ResetInferenceEngine(), message: data.action + " has been executed." });
            break;
        case "loadCSV":
            FS.writeFile("tmp.csv", data.contents, { encoding: "utf8", flags: "w" });
            workerSendMsg({
                id: data.id,
                action: data.action,
                string: BN.LoadCSV("tmp.csv", data.bn_name),
                message: data.action + " has been executed."
            });
            break;
        case "getAllLists":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetAllLists(), message: data.action + " has been executed." });
            break;
        case "chgEvidence":
            workerSendMsg({ id: data.id, action: data.action, string: BN.ChgEvidence(data.nodeId, data.evidence), message: data.action + " has been executed." });
            break;
        case "eraseEvidence":
            workerSendMsg({ id: data.id, action: data.action, string: BN.EraseEvidence(data.nodeId), message: data.action + " has been executed." });
            break;
        case "hardEvidence":
            workerSendMsg({ id: data.id, action: data.action, string: BN.HardEvidence(), message: data.action + " has been executed." });
            break;
        case "moveNodeInf":
            workerSendMsg({ id: data.id, action: data.action, string: BN.MoveNodeInf(data.nodeId, data.x, data.y), message: data.action + " has been executed." });
            break;
        case "getPosNodeInf":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetPosNodeInf(data.nodeId), message: data.action + " has been executed." });
            break;
        case "setGroupNode":
            workerSendMsg({ id: data.id, action: data.action, string: BN.SetGroupNode(data.nodeId, data.group), message: data.action + " has been executed." });
            break;
        case "getGroupNode":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetGroupNode(data.nodeId), message: data.action + " has been executed." });
            break;
        case "getName":
            workerSendMsg({ id: data.id, action: data.action, string: BN.GetName(), message: data.action + " has been executed." });
            break;
        default:
            console.log('Worker says: no action called "' + data.action + '".');
            break;
    }
};

// list of API functions
// .function("AddNode", &JSBayesNet<double>::AddNode)
// .function("RemoveNode", &JSBayesNet<double>::RemoveNode)
// .function("GetNodesList", &JSBayesNet<double>::GetNodesList)
// .function("AddArc", &JSBayesNet<double>::AddArc)
// .function("RemoveArc", &JSBayesNet<double>::RemoveArc)
// .function("GetArcsList", &JSBayesNet<double>::GetArcsList)
// .function("ToString", &JSBayesNet<double>::ToString)
// .function("ToDot", &JSBayesNet<double>::ToDot)
// .function("GeneraCTPs", &JSBayesNet<double>::GeneraCTPs)
// .function("Cpt", &JSBayesNet<double>::Cpt)
// .function("AddCpt", &JSBayesNet<double>::AddCpt)
// .function("MoveNode", &JSBayesNet<double>::MoveNode)
// .function("GetPosNode", &JSBayesNet<double>::GetPosNode)
// .function("GetAllPosNode", &JSBayesNet<double>::GetAllPosNode)
// .function("LoadBN", &JSBayesNet<double>::LoadBN)
// .function("SaveBN", &JSBayesNet<double>::SaveBN)
// .function("LoadBNPos", &JSBayesNet<double>::LoadBNPos)
// .function("SaveBNPos", &JSBayesNet<double>::SaveBNPos)
// .function("Posterior", &JSBayesNet<double>::Posterior)
// .function("AddEvidence", &JSBayesNet<double>::AddEvidence)
// .function("ComputeInference", &JSBayesNet<double>::ComputeInference)
// .function("ResetInferenceEngine", &JSBayesNet<double>::ResetInferenceEngine)
// .function("LoadCSV", &JSBayesNet<double>::LoadCSV)
// .function("GetAllLists", &JSBayesNet<double>::GetAllLists)
// .function("ChangeNodeName", &JSBayesNet<double>::ChangeNodeName)
// .function("ChangeLabelsName", &JSBayesNet<double>::ChangeLabelsName)
// .function("ChgEvidence", &JSBayesNet<double>::ChgEvidence)
// .function("EraseEvidence", &JSBayesNet<double>::EraseEvidence)
// .function("HardEvidence", &JSBayesNet<double>::HardEvidence)
// .function("MoveNodeInf", &JSBayesNet<double>::MoveNodeInf)
// .function("GetPosNodeInf", &JSBayesNet<double>::GetPosNodeInf)
// .function("SetGroupNode", &JSBayesNet<double>::SetGroupNode)
// .function("GetGroupNode", &JSBayesNet<double>::GetGroupNode)
// .function("GetName",  &JSBayesNet<double>::GetName)

/**
 * Handles errors
 */
onerror = function(err) {
    BN.delete();
    BN = undefined;
    console.log("[ Worker Error ] : BN deleted");
    console.log("[ Worker Error ] : Worker says : ", err);
    workerSendMsg({ action: "ErrorMode", message: "worker in Error" });
};

/** linked function in NodeJS  */
if (NODE_VERSION) {
    parentPort.on('message', onmessage);
    parentPort.on('error', onerror);
}