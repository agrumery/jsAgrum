//Module.TOTAL_MEMORY = 268435456;

/** @module Worker Utilities */

/** defines the platform NODE_VERSION is nodeJs => !NODE_VERSION is web*/
var NODE_VERSION = typeof process === "object" && typeof process.versions === "object" && typeof process.versions.node === "string";


const { parentPort } = require('worker_threads');

/**
 * Abstract NodeJS / Web cases
 * @param {hash} msg message to send.
 */
function workerSendMsg(msg) {
    if (NODE_VERSION) {
        parentPort.postMessage(msg);
    } else {
        postMessage(msg);
    }
}


var Module = {
    // functions executed at this end of the initialization of the worker
    onRuntimeInitialized: function() {
        console.log("libagrum.js loaded ...");
        workerSendMsg({ action: "loaded", message: "worker loaded" });
    }
};