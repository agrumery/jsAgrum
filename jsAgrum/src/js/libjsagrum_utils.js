//!\ These functions are to be used on the main thread (not the worker)

/** @module Load Promises Utilities */

// Global variables for the Promises Wrapper
/** keeps the resolve and reject info  */
const resolves = {};
const rejects = {};
/** keeps the Unique Id */
let globalMsgId = 0;
/** Keeps the function to call when the worker is loaded */
let loadedCallBack;
/** Keeps the function to call when the worker is error */
let workerErrorCallBack;
/** Keeps the function to call when the browser doesn't support worker */
let noWorkerCallBack;

/**
 * Wrapped the Message and send it with the promise
 * @param {hash} payload  payload to send.
 * @param {object} worker worker value.
 */
function sendMsg(payload, worker) {
    const msgId = globalMsgId++;
    const msg = {
        id: msgId
    };
    Object.assign(msg, payload);
    return new Promise(function(resolve, reject) {
        // save callbacks for later
        resolves[msgId] = resolve;
        rejects[msgId] = reject;

        worker.postMessage(msg);
    });
}

/**
 * Internal function that resolve the promise
 * @param {hash} msg received message. 
 */
function handleMsg(msg) {

    if (msg.data.action == "loaded") {
        loadedCallBack();
    } else if (msg.data.action == "ErrorMode") {
        workerErrorCallBack();
    } else if (msg.data.message) {
        let mes = [];
        if (msg.data.string) {
            mes = msg.data.string.split(":");
        }
        if (mes[0] == "Error") {
            const reject = rejects[msg.data.id];
            if (reject) {
                if (mes[1]) {
                    reject(mes[1]);
                }
            }
        } else {
            const resolve = resolves[msg.data.id];
            if (resolve) {
                resolve(msg.data);
            }
        }
    } else {
        // error condition
        const reject = rejects[msg.data.id];
        if (reject) {
            if (msg.data.err) {
                reject(msg.data.err);
            } else {
                reject('Got nothing');
            }
        }
    }

    // purge used callbacks
    delete resolves[msg.data.id];
    delete rejects[msg.data.id];
}

/**
 * Wrapper Class Declaration
 */
class Wrapper {
    constructor() {
        if (typeof(Worker) !== "undefined") {
            this.worker = new Worker('../../jsAgrum/lib/libagrum.js')
            this.worker.onmessage = handleMsg
        } else {
            noWorkerCallBack();
        }
    }

    oche(str) {
        return sendMsg(str, this.worker)
    }
}

// Global variables
/** Keeps the content of the BN file */
var bifContent = null;
/** Keeps the content of the Json file */
var jsonContent = null;
/** Keeps the type file */
var typeContent = null;
/** Keeps the file name */
var nameFile = null;


/**
 * Handles the VBN format, read the files before sending the command
 * @param {object} zip Zip Object
 */
function unZipVbn(zip) {
    //reset data
    bifContent = jsonContent = typeContent = nameFile = null;
    zip.forEach(function(relativePath, zipEntry) {
        console.log(zipEntry.name);
        if (zipEntry.name.includes(".jusu")) {
            zip.file(zipEntry.name).async("string").then(function(text) {
                if (jsonContent == null) {
                    jsonContent = text;
                }
                loadRefreshCallBack();

            });
        } else if (zipEntry.name.includes(".bifxml")) { // before bif because it contains .bif inside .bifxml
            zip.file(zipEntry.name).async("string").then(function(text) {
                if (bifContent == null) {
                    bifContent = text;
                    typeContent = 4; //bifxml
                    nameFile = zipEntry.name;
                }
                loadRefreshCallBack();

            });
        } else if (zipEntry.name.includes(".bif")) {
            zip.file(zipEntry.name).async("string").then(function(text) {
                if (bifContent == null) {
                    bifContent = text;
                    typeContent = 1; //bif
                    nameFile = zipEntry.name;
                }
                loadRefreshCallBack();

            });
        } else if (zipEntry.name.includes(".net")) {
            zip.file(zipEntry.name).async("string").then(function(text) {
                if (bifContent == null) {
                    bifContent = text;
                    typeContent = 2; //net
                    nameFile = zipEntry.name;
                }
                loadRefreshCallBack();

            });
        } else if (zipEntry.name.includes(".o3prm")) {
            zip.file(zipEntry.name).async("string").then(function(text) {
                if (bifContent == null) {
                    bifContent = text;
                    typeContent = 3; //o3prm
                    nameFile = zipEntry.name;
                }
                loadRefreshCallBack();

            });
        } else if (zipEntry.name.includes(".dsl")) {
            zip.file(zipEntry.name).async("string").then(function(text) {
                if (bifContent == null) {
                    bifContent = text;
                    typeContent = 5; //dsl
                    nameFile = zipEntry.name;
                }
                loadRefreshCallBack();

            });
        } else {
            console.log(" Error VBN contain wrong files (" + zipEntry.name + ")");
        }

    });
}

/**
 * Unzips files from the raw data
 * @param {string} data File content 
 */
function loadVBNFromData(data) {
    JSZip.loadAsync(data)
        .then(function(zip) {
            unZipVbn(zip);
        }, function(e) {
            console.log(e);
        });
}

/**
 * Recovers the VBN data from an URL
 * @param {string} addr URL to recover 
 */
function loadVBNFromAdrr(addr) {
    JSZipUtils.getBinaryContent(addr, function(err, data) {
        if (err) {
            console.log("error getBinary");
            console.log(err);
            throw err;
        }
        loadVBNFromData(data);
    });
}

/**
 * Recovers BN from URL and sends the command to load
 * @param {string} addr URL to recover 
 */
function loadBNFromAdrr(addr, type) {
    var txtFile = new XMLHttpRequest();
    txtFile.open("GET", addr, true);
    txtFile.onreadystatechange = function() {
        if (txtFile.readyState === 4) { // Makes sure the document is ready to parse.
            if (txtFile.status === 200) { // Makes sure it's found the file.
                loadBN("tmp", txtFile.responseText, "{}", type);
            }
        }
    }
    txtFile.send(null);
}


/**  wrapper declaration */
const wrapper = new Wrapper();

// List of all the command functions to the jsAgrum Library
// This version proposes the command with promises
/** @module Promise Command list */

/**
 * Ask for the creation a new BN object
 * @param {string} name Chosen name
 */
var createBN = function(name) {
    let msg = {
        action: "createBN",
        name: name
    };
    return wrapper.oche(msg);
};

/**
 * Ask to obtain the BN graph as a string
 */
var toStringBN = function() {
    let msg = {
        action: "toString"
    };
    return wrapper.oche(msg);
};

/**
 * Ask to obtain the BN graph as a dot file
 */
var toDot = function() {
    let msg = {
        action: "toDot"
    };
    return wrapper.oche(msg);
};

/**
 * Ask to add a node
 * @param {string} name Chosen name. 
 * @param {integer} nbrmod Number of mode. 
 * @param {integer} posX X position.
 * @param {integer} posY Y position.
 */
var addNode = function(name, nbrmod, posX, posY) {
    let msg = {
        action: "addNode",
        name: name,
        nbrmod: nbrmod,
        posX: posX,
        posY: posY
    };
    return wrapper.oche(msg);
};

/**
 * Ask to remove a node
 * @param {integer} nodeId Node Id. 
 */
var removeNode = function(nodeId) {
    let msg = {
        action: "removeNode",
        nodeId: nodeId
    };
    return wrapper.oche(msg);
};

/**
 * Ask to change a node name
 * @param {integer} nodeId Node Id. 
 * @param {string} name New Name. 
 */
var changeNodeName = function(nodeId, name) {
    let msg = {
        action: "changeNodeName",
        nodeId: nodeId,
        name: name
    };
    return wrapper.oche(msg);
};

/**
 * Ask to change the mode/Label names
 * @param {integer} nodeId Node Id.
 * @param {json} jsontxt List of names.
 */
var changeLabelsName = function(nodeId, jsontxt) {
    let msg = {
        action: "changeLabelsName",
        nodeId: nodeId,
        jsontxt: jsontxt
    };
    return wrapper.oche(msg);
}

/**
 * Ask to add a Arc between two nodes
 * @param {integer} tail Tail node Id. 
 * @param {integer} head Head node Id.
 */
var addArc = function(tail, head) {
    let msg = {
        action: "addArc",
        tail: tail,
        head: head
    };
    return wrapper.oche(msg);
};

/**
 * Ask to remove an Arc between two nodes
 * @param {integer} tail Tail node Id.
 * @param {integer} head Head node Id.
 */
var removeArc = function(tail, head) {
    let msg = {
        action: "removeArc",
        tail: tail,
        head: head
    };
    return wrapper.oche(msg);
};

/**
 * Ask to generate random Cpts for all nodes
 */
var genCPTs = function() {
    let msg = {
        action: "generateCpts"
    };
    return wrapper.oche(msg);
};

/**
 * Ask to obtain cpt values as json
 * @param {integer} nodeId Node Id  
 */
var cpt = function(nodeId) {
    let msg = {
        action: "cpt",
        nodeId: nodeId
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain the node list as json
 */
var getNodesList = function() {
    let msg = {
        action: "getNodesList"
    };
    return wrapper.oche(msg);
};

/**
 * Ask to obtain the arc list as json
 */
var getArcsList = function() {
    let msg = {
        action: "getArcsList"
    };
    return wrapper.oche(msg);
};

/**
 * Ask to add cpt to a node
 * @param {integer} nodeId Node id. 
 * @param {integer} jsontxt Cpt as json list
 */
var addCpt = function(nodeId, jsontxt) {
    let msg = {
        action: "addCpt",
        nodeId: nodeId,
        jsontxt: jsontxt
    };
    return wrapper.oche(msg);
}

/**
 * Ask to move a node
 * @param {integer} nodeId Node Id. 
 * @param {integer} posX X position.
 * @param {integer} posY Y position.
 */
var moveNode = function(nodeId, posX, posY) {
    let msg = {
        action: "moveNode",
        nodeId: nodeId,
        x: posX,
        y: posY
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain the position of a node as json
 * @param {integer} nodeId Node Id. 
 */
var getPosNode = function(nodeId) {
    let msg = {
        action: "getPosNode",
        nodeId: nodeId
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain the position node list as json
 */
var getAllPosNode = function() {
    let msg = {
        action: "getAllPosNode"
    };
    return wrapper.oche(msg);
}

/**
 * Ask to load a new BN
 * @param {string} name Name file.
 * @param {string} content BN File content.
 * @param {string} contentpos Json File content.
 * @param {integer} type Type file.
 */
var loadBN = function(name, content, contentpos, type) {
    let msg = {
        action: "loadBN",
        name: name,
        contents: content,
        posfile: contentpos,
        type: type
    };
    return wrapper.oche(msg);
}

/**
 * Ask to save the BN as VBN 
 */
var saveBN = function() {
    let msg = {
        action: "saveBN"
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain posterior to a node
 * @param {integer} nodeId Node Id. 
 */
var posterior = function(nodeId) {
    let msg = {
        action: "posterior",
        nodeId: nodeId
    };
    return wrapper.oche(msg);
}

/**
 * Ask to add hard evidence to a node
 * @param {integer} nodeId Node Id. 
 * @param {integer} value Label Id.
 */
var addEvidence = function(nodeId, value) {
    let msg = {
        action: "addEvidence",
        nodeId: nodeId,
        evidence: value
    };
    return wrapper.oche(msg);
}

/**
 * Ask to change a hard evidence to a node
 * @param {integer} nodeId Node Id.
 * @param {integer} value New label Id. 
 */
var chgEvidence = function(nodeId, value) {
    let msg = {
        action: "chgEvidence",
        nodeId: nodeId,
        evidence: value
    };
    return wrapper.oche(msg);
}

/**
 * Ask to erase a hard evidence to a node
 * @param {integer} nodeId Node Id. 
 */
var eraseEvidence = function(nodeId) {
    let msg = {
        action: "eraseEvidence",
        nodeId: nodeId
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain the hard evidence list
 */
var hardEvidence = function() {
    let msg = {
        action: "hardEvidence"
    };
    return wrapper.oche(msg);
}

/**
 * Ask to reset the evidence engine
 */
var resetEvidenceEngine = function() {
    let msg = {
        action: "resetEvidenceEngine"
    };
    return wrapper.oche(msg);
}

/**
 * Ask to create a BN from a CSV file
 * @param {string} content CSV File content
 * @param {string} bn_name Name of the new created BN 
 */
var loadCSV = function(content, bn_name) {
    let msg = {
        action: "loadCSV",
        contents: content.replace(/"/g, ''),
        bn_name: bn_name
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain Node, Arc and Position list as json
 */
var refreshGraph = function() {
    let msg = {
        action: "getAllLists"
    };
    return wrapper.oche(msg);
}

/**
 * Ask to move a node in inference mod
 * @param {integer} nodeId Node Id. 
 * @param {integer} posX X position.
 * @param {integer} posY Y position.
 */
var moveNodeInf = function(nodeId, posX, posY) {
    let msg = {
        action: "moveNodeInf",
        nodeId: nodeId,
        x: posX,
        y: posY
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain the position in inference mod of a node as json
 * @param {integer} nodeId Node Id. 
 */
var getPosNodeInf = function(nodeId) {
    let msg = {
        action: "getPosNodeInf",
        nodeId: nodeId
    };
    return wrapper.oche(msg);
}

/**
 * Ask to set a group to a node
 * @param {integer} nodeId Node Id. 
 * @param {integer} group group number
 */
var setGroupNode = function(nodeId, group) {
    let msg = {
        action: "setGroupNode",
        nodeId: nodeId,
        group: group
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain the group of a node as json
 * @param {integer} nodeId Node Id. 
 */
var getGroupNode = function(nodeId) {
    let msg = {
        action: "getGroupNode",
        nodeId: nodeId
    };
    return wrapper.oche(msg);
}

/**
 * Ask to obtain the name of the BN 
 */
var getName = function() {
    let msg = {
        action: "getName",
    };
    return wrapper.oche(msg);
}