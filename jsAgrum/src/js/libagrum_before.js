//Module.TOTAL_MEMORY = 268435456;

/** @module Worker Utilities */

var NODE_VERSION = typeof process === "object" && typeof process.versions === "object" && typeof process.versions.node === "string";

/**
 * pings an agrum server to count usage
 */
function ping() {
    /*
      $.ajax({
          url: 'https://webia.lip6.fr/~phw/aGrUM/ping.php',
          success: function(result) {},
          error: function(result) {}
      });
      */
    const Http = new XMLHttpRequest();
    const url = 'https://webia.lip6.fr/~phw/aGrUM/ping.php';
    Http.open("GET", url);
    Http.send();

    Http.onreadystatechange = function() {
        console.log(Http.responseText);
    }
}

/**
 * Abstract NodeJS / Web cases
 * @param {hash} msg message to send.
 */
function workerSendMsg(msg) {
    if (NODE_VERSION) {
        parentPort.postMessage(msg);
    } else {
        postMessage(msg);
    }
}


var Module = {
    // functions executed at this end of the initialization of the worker
    onRuntimeInitialized: function() {
        console.log("libagrum.js loaded ...");
        workerSendMsg({ action: "loaded", message: "worker loaded" });
        // ping the agrum server each time jsAgrum is loaded
        ping();

    }
};