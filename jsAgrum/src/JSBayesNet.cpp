#define NDEBUG 1

#include "JSBayesNet.h"
#include <iostream>
#include <fstream>
#include <sstream> // std::ostringstream

// Save/Load include
#include <agrum/BN/io/BIF/BIFReader.h>
#include <agrum/BN/io/BIF/BIFWriter.h>
#include <agrum/BN/io/net/netReader.h>
#include <agrum/PRM/o3prm/O3prmBNReader.h>
#include <agrum/PRM/o3prm/O3prmBNWriter.h>
#include <agrum/BN/io/DSL/DSLReader.h>
#include <agrum/BN/io/BIFXML/BIFXMLBNReader.h>

#include "nlohmann/json.hpp"
using json = nlohmann::json;

using namespace gum;

template <class T>
JSBayesNet<T>::JSBayesNet(const std::string &name)
    : name(name), _bn_instance(name), _inference_engine(nullptr)
{

#ifdef NDEBUG
    std::cerr << "CONSTRUCTOR JSBayesNet" << std::endl;
#endif

    _inference_engine = new LazyPropagation<T>(&_bn_instance);

    posXT = _bn_instance.dag().nodesProperty(0);
    posYT = _bn_instance.dag().nodesProperty(0);
    posXTInf = _bn_instance.dag().nodesProperty(0);
    posYTInf = _bn_instance.dag().nodesProperty(0);
    group = _bn_instance.dag().nodesProperty(0);
}

template <class T>
JSBayesNet<T>::~JSBayesNet()
{
#ifdef NDEBUG
    std::cerr << "DESTRUCTION JSBayesNet" << std::endl;
#endif
    if (_db != nullptr)
    {
        delete _db;
        _db = nullptr;
    }

    if (_inference_engine != nullptr)
    {
        delete _inference_engine;
        _inference_engine = nullptr;
    }
}

template <class T>
std::string JSBayesNet<T>::AddNode(const std::string &name, 
                                   const unsigned int nbrmod,
                                   const unsigned int posX,
                                   const unsigned int posY)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "ADD NODE" << std::endl;
#endif

    try
    {
        auto varId = _bn_instance.add(name, nbrmod);
        posXT.set(varId, posX);
        posYT.set(varId, posY);
        posXTInf.set(varId, posX);
        posYTInf.set(varId, posY);
        os << varId << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::RemoveNode(const unsigned int varId)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "ERASE NODE" << std::endl;
#endif

    try
    {
        _bn_instance.erase(varId);
        posXT.erase(varId);
        posYT.erase(varId);
        posXTInf.erase(varId);
        posYTInf.erase(varId);
        group.erase(varId);
        os << "erase node" << varId << " done" << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::ChangeNodeName(const unsigned int varId, const std::string &name)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "CHANGE NODE NAME : " << varId << " " << name << std::endl;
#endif

    try
    {
        _bn_instance.changeVariableName(varId, name);
        os << "change node name" << varId << " done" << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::ChangeLabelsName(const unsigned int varId, const std::string &jsontxt){
std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "CHANGE LABELS NAME (" <<varId << ")"<< std::endl;
#endif

    try
    {
        // Parse the string Arg in Json
        json j = json::parse(jsontxt.c_str());
#ifdef NDEBUG
        std::cout << j.dump() << std::endl;
#endif

        for (auto& el : j.items()) 
        {
            std::string old_name = el.key();
            std::string new_name = el.value();
            _bn_instance.changeVariableLabel(varId, old_name, new_name); 
        }

        os << "change node labels name" << varId << " done" << std::endl;
    }
    catch (NotFound e) 
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GetNodesList(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "GET NODES LIST" << std::endl;
#endif

    try
    {
        json j = {};

        for (auto nodeId: _bn_instance.nodes()){
            j["NodesList"][nodeId]["id"] = nodeId;
            j["NodesList"][nodeId]["name"] = _bn_instance.variable(nodeId).name();

            int i = 0;
            for (auto label : _bn_instance.variable(nodeId).labels())
            {
                j["NodesList"][nodeId]["labels"][i] = label;
                i++;
            }  
        }

        os << j.dump();
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::MoveNode(const unsigned int varId, const int posX, const int posY)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "MOVE NODE " << varId << " (" << posX << "," << posY << ")" << std::endl;
#endif

    posXT.set(varId, posX);
    posYT.set(varId, posY);

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GetPosNode(const unsigned int varId)
{

    std::ostringstream os;
    json j = {};

#ifdef NDEBUG
    std::cerr << "GET NODE POS" << varId << std::endl;
#endif

    int posX = posXT.getWithDefault(varId, 0);
    int posY = posYT.getWithDefault(varId, 0);

    j["posXY"] = {posX, posY};

    os << j.dump();

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::MoveNodeInf(const unsigned int varId, const int posX, const int posY)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "MOVE NODE INF" << varId << " (" << posX << "," << posY << ")" << std::endl;
#endif

    posXTInf.set(varId, posX);
    posYTInf.set(varId, posY);

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GetPosNodeInf(const unsigned int varId)
{

    std::ostringstream os;
    json j = {};

#ifdef NDEBUG
    std::cerr << "GET NODE POS INF" << varId << std::endl;
#endif

    int posX = posXTInf.getWithDefault(varId, 0);
    int posY = posYTInf.getWithDefault(varId, 0);

    j["posXYInf"] = {posX, posY};

    os << j.dump();

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::SetGroupNode(const unsigned int varId, const int new_group)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "SET GROUP NODE" << varId << " (" << new_group << ")" << std::endl;
#endif

    group.set(varId, new_group);

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GetGroupNode(const unsigned int varId)
{

    std::ostringstream os;
    json j = {};

#ifdef NDEBUG
    std::cerr << "GET GROUP NODE" << varId << std::endl;
#endif

    int gr = group.getWithDefault(varId, 0);

    j["grNode"] = gr;

    os << j.dump();

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GetAllPosNode(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "GET ALL NODE POS" << std::endl;
#endif

    json j = {};

    for (auto nodeId: _bn_instance.nodes()){
        j["NodesPosList"][nodeId]["x"] = posXT.getWithDefault(nodeId, 0);
        j["NodesPosList"][nodeId]["y"] = posYT.getWithDefault(nodeId, 0);
        j["NodesInfPosList"][nodeId]["x"] = posXTInf.getWithDefault(nodeId, 0);
        j["NodesInfPosList"][nodeId]["y"] = posYTInf.getWithDefault(nodeId, 0);
    }

    os << j.dump();

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::AddArc(const unsigned int tailId, const unsigned int headId)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "ADD ARC (" << tailId << "->" << headId << ")" << std::endl;
#endif

    try
    {
        _bn_instance.addArc(tailId, headId);
        os << "added arc (" << tailId << "->" << headId << ") done" << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::RemoveArc(const unsigned int tailId, const unsigned int headId)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "ERASE ARC (" << tailId << "->" << headId << ")" << std::endl;
#endif

    try
    {
        _bn_instance.eraseArc(tailId, headId);
        os << "erased arc (" << tailId << "->" << headId << ") done" << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GetArcsList(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "GET ARCS LIST" << std::endl;
#endif
    try
    {
        json j = {};
        int i = 0;
        for (const auto arc : _bn_instance.dag().arcs())
        {
            j["ArcsList"][i]["tail"] = arc.tail();
            j["ArcsList"][i]["head"] = arc.head();
            i++;
        }

        os << j.dump();
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::ToString(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "BAYES NET TO STRING" << std::endl;
#endif
    try
    {
        os << _bn_instance.toString();
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::ToDot(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "BAYES NET TO DOT" << std::endl;
#endif

    try
    {
        os << _bn_instance.toDot();
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::PosteriorAsHTML(const Potential<T> &p)
{
    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "POSTERIOR AS HTML" << std::endl;
#endif

    try
    {
        // retrieve variable names and reverse it
        std::vector<std::string> var_names;
        auto seq = p.variablesSequence();
        std::for_each(seq.begin(), seq.end(), [&var_names](auto &var) { var_names.push_back(var->name()); });
        std::reverse(std::begin(var_names), std::end(var_names));

        // retrieve instantiation
        Instantiation inst = _bn_instance.completeInstantiation();

        os << "<table>";
        if (p.empty())
        {
            os << "<tr><th style=\"background-color:#AAAAAA\">&nbsp;</th></tr>"
               << "<tr>" + _mkCell(p.get(inst), 4, true) + "</tr>";
        }
        else
        {
            Idx nparents = p.nbrDim() - 1;
            auto &var = p.variable(0);
            std::string varname(var.name());

            // first line
            if (nparents > 0)
            {
                os << "<tr><th colspan=\"" << nparents
                   << "\"></th><th colspan=\"" << var.domainSize()
                   << "\" style=\"background-color:#AAAAAA\"><center>" << varname << "</center></th></tr>";
            }
            else
            {
                os << "<tr style=\"background-color:#AAAAAA\"><th colspan=\"" << var.domainSize()
                   << "\"><center>" << varname << "</center></th></tr>";
            }

            // second line
            os << "<tr>";
            if (nparents > 0)
            {
                for (int i = nparents - 1; i > -1; i -= 1)
                {
                    std::string parent(var_names[i]);
                    os << "<th style=\"background-color:#AAAAAA\"><center>" << parent << "</center></th>";
                }
            }
            std::vector<std::string> labels = var.labels();
            std::for_each(labels.begin(), labels.end(), [&os](auto &label) {
                os << "<th style=\"background-color:#BBBBBB\"><center>" << label << "</center></th>";
            });
            os << "</tr>";

            inst = Instantiation(p);
            int off = 1;
            std::map<int, int> offset;
            for (int j = 1; j != nparents + 1; j++)
            {
                offset[j] = off;
                off *= inst.variable(j).domainSize();
            }
            inst.setFirst();
            while (!inst.end())
            {
                os << "<tr>";
                for (int par = 1; par != nparents + 1; par++)
                {
                    if (par == 1)
                    {
                        os << "<th style=\"background-color:#BBBBBB\"><center>"
                           << inst.variable(par).label(inst.val(par)) << "</center></th>";
                    }
                    else
                    {
                        int sum = 0;
                        for (int i = 1; i != par; i++)
                        {
                            sum += inst.val(i);
                        }

                        if (sum == 0)
                        {
                            os << "<th style=\"background-color:#BBBBBB;\" rowspan=\""
                               << offset[par] << "\"><center>"
                               << inst.variable(par).label(inst.val(par)) << "</center></th>";
                        }
                    }
                }
                for (int i = 0; i < p.variable(0).domainSize(); i++)
                {
                    os << _mkCell(p.get(inst), 4, true);
                    inst.inc();
                }
                os << "</tr>";
            }
        }
        os << "</table>";
    }
    catch (NotFound e)
    {
        std::cerr << e.what() << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
    }
    return os.str();
}

template <class T>
std::string JSBayesNet<T>::PosteriorAsJson(const Potential<T> &p)
{
    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "POSTERIOR AS JSON" << std::endl;
#endif

    try
    {
        json j = {};

        // retrieve name, domainSize and labels of node and parents
        int i = 0;
        for (const auto var : p.variablesSequence())
        {
            if (i==0) {
                j["node"]["name"] = var->name();
                j["node"]["size"] = var->domainSize();
                j["node"]["labels"] = var->labels();    
            } else {
                j["parents"][i-1]["name"] = var->name();
                j["parents"][i-1]["size"] = var->domainSize();
                j["parents"][i-1]["labels"] = var->labels();
            }
            
            i++;
        }

        // retrieve instantiation
        Instantiation inst(p);
        // set first
        inst.setFirst();
        i = 0;
        // copy
        while (!inst.end())
        {
            j["data"][i] = p.get(inst);
            inst.inc();
            i++;
        }
        os << j.dump();

    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }
    return os.str();
}

template <class T>
std::string JSBayesNet<T>::Cpt(const unsigned int varId)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "CPT NODE " << varId << std::endl;
#endif

    // Potential of variable
    try
    {
        Potential<T> p = _bn_instance.cpt(varId);
        //os << PosteriorAsHTML(p);
        os << PosteriorAsJson(p);
#ifdef NDEBUG
        std::cerr << "Cpt Node" << varId << " : " << p << std::endl;
#endif
    }
    catch (Exception e)
    {
        os << "Error:" << e.errorType() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GeneraCTPs(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "GENERATE RANDOM CTP" << std::endl;
#endif

    try
    {
        _bn_instance.generateCPTs();
        os << "generate random CPT done" << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::AddCpt(const unsigned int varId, const std::string &jsontxt)
{
    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "ADD CPT VALUES " << varId << std::endl;
#endif

    try
    {
        // Parse the string Arg in Json
        json j = json::parse(jsontxt.c_str());
#ifdef NDEBUG
        std::cout << j.dump() << std::endl;
#endif
        // Vector of potential value contruction
        std::vector<T> values;
        for (auto v : j["pValues"])
        {
            values.push_back((T)v);
        }

        // BN assimilation
        _bn_instance.cpt(varId).fillWith(values);
        _bn_instance.cpt(varId).normalizeAsCPT();
        os << "added cpt to " << varId << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::LoadBN(const std::string &nameF, const unsigned int type)
{
    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "LOAD BN (type: " << type << ")" << std::endl;
#endif

    try
    {
        // Clean the instance before adding a new graph
        if (!_bn_instance.empty())
        {
            auto l = _bn_instance.nodes();
            for (const auto no : l)
            {
                //_bn_instance.erase(no);
                RemoveNode(no);
            }
        }

        switch (type)
        {
            case 1: {//bif
                std::cout << "BIF File" << std::endl;
                auto reader = BIFReader<T>(&_bn_instance, nameF);
                reader.proceed();
                reader.showElegantErrorsAndWarnings(os);
                break;
            }
            case 2: {//net
                std::cout << "NET File" << std::endl;
                auto reader = NetReader<T>(&_bn_instance, nameF);
                reader.proceed();
                reader.showElegantErrorsAndWarnings(os);
                break;
            }
            case 3: {//o3prm
                std::cout << "O3PRM File" << std::endl;
                auto reader = O3prmBNReader<T>(&_bn_instance, nameF, "", ".");         
                reader.proceed(); 
                reader.showElegantErrorsAndWarnings(os);
                break;
            }    
            case 4: {//bifxml
                std::cout << "BIFXML File" << std::endl;
                auto reader = BIFXMLBNReader<T>(&_bn_instance, nameF);
                reader.proceed();
                break;
            }   
            case 5: {//dsl
                std::cout << "DSL File" << std::endl;
                auto reader = DSLReader<T>(&_bn_instance, nameF);
                reader.proceed();
                reader.showElegantErrorsAndWarnings(os);
                break;
            }
            default: {
                os << "Error: file type not found" << std::endl;
                break;
            }
        }
        
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    name = _bn_instance.property("name");

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::SaveBN(const std::string &name)
{
    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "SAVE BN" << std::endl;
#endif

    try
    {
        //auto writer = BIFWriter<T>();
        O3prmBNWriter<T> writer;
        writer.write(name, _bn_instance);
        os << "write file : " << name << std::endl;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::LoadBNPos(const std::string &jsonTxt)
{
    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "LOAD BN position" << std::endl;
    std::cerr << jsonTxt << std::endl;
#endif

    try
    {
        json j = json::parse(jsonTxt.c_str());
        posXT.clear();
        posYT.clear();
        posXTInf.clear();
        posYTInf.clear();
        group.clear();

        for (auto &el : j.items())
        {
            try {
                unsigned int node = _bn_instance.idFromName(el.key());

                posXT.set(node, j[el.key()]["X"]);
                posYT.set(node, j[el.key()]["Y"]);
                if (j[el.key()]["InfX"] != nullptr) {
                    posXTInf.set(node, j[el.key()]["InfX"]);
                } else {
                    posXTInf.set(node, j[el.key()]["X"]);
                }
                if (j[el.key()]["InfX"] != nullptr) {
                    posYTInf.set(node, j[el.key()]["InfY"]);
                } else {
                    posYTInf.set(node, j[el.key()]["Y"]);
                }
                if (j[el.key()]["group"] != nullptr) {
                    group.set(node, j[el.key()]["group"]);
                }
                

            } catch (NotFound e)
            {
                std::cerr << e.what() << std::endl;
            }
        }

        os << "load position done";
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::SaveBNPos(const std::string &name)
{
    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "SAVE BN position" << std::endl;
#endif

    try
    {
        json j = {};
        std::ofstream myfile(name);

        for (auto nodeId: _bn_instance.nodes()){
            j[_bn_instance.variable(nodeId).name()]["X"] = posXT.getWithDefault(nodeId,0);
            j[_bn_instance.variable(nodeId).name()]["Y"] = posYT.getWithDefault(nodeId,0);
            j[_bn_instance.variable(nodeId).name()]["InfX"] = posXTInf.getWithDefault(nodeId,0);
            j[_bn_instance.variable(nodeId).name()]["InfY"] = posYTInf.getWithDefault(nodeId,0);
            j[_bn_instance.variable(nodeId).name()]["group"] = group.getWithDefault(nodeId,0);
        }

        if (myfile.is_open())
        {
            myfile << j.dump();
            os << "write file : " << name << std::endl;
            myfile.close();
        }
        else
        {
            os << "Error: writing file - " << name << std::endl;
        }
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::Posterior(const unsigned int varId)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "POSTERIOR : " << varId << std::endl;
#endif

    try
    {
        _inference_engine->eraseAllTargets();
        _inference_engine->addTarget(varId);

        if (_inference_engine->isInferenceOutdatedBNPotentials())
            std::cerr << "ie outdated potentials" << std::endl;

        // recompute inference
        ComputeInference();

        Potential<T> p = _inference_engine->posterior(varId);

        os << PosteriorAsJson(p);

        //os << PosteriorAsHTML(p);
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::ResetInferenceEngine(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "RESET INFERENCE ENGINE " << std::endl;
#endif

    try
    {
        if (_inference_engine != nullptr)
            delete (_inference_engine);
        _inference_engine = new LazyPropagation<T>(&_bn_instance);
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::AddEvidence(const unsigned int varId, const unsigned int eviValue)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "ADD EVIDENCE : " << varId << std::endl;
#endif

    try
    {
        if (_inference_engine->hasEvidence(varId))
        {
            _inference_engine->chgEvidence(varId, eviValue);
        } else {
            _inference_engine->addEvidence(varId, eviValue);
        }
        
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::ChgEvidence(const unsigned int varId, const unsigned int eviValue)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "CHANGE EVIDENCE : " << varId << ": " << eviValue << std::endl;
#endif

    try
    {
        _inference_engine->chgEvidence(varId, eviValue);
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::EraseEvidence(const unsigned int varId)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "ERASE EVIDENCE : " << varId << std::endl;
#endif

    try
    {
        _inference_engine->eraseEvidence(varId);
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::HardEvidence(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "GET HARD EVIDENCE LIST" << std::endl;
#endif

    try
    {
        json j = {};

        auto nodeSet = _inference_engine->hardEvidenceNodes();
        auto nodeSetValue = _inference_engine->hardEvidence();

        for (auto nodeId : nodeSet)
        {
            j["NodesEvidenceList"][nodeId]["id"] = nodeId;
            j["NodesEvidenceList"][nodeId]["name"] = _bn_instance.variable(nodeId).name();
            j["NodesEvidenceList"][nodeId]["value"] = nodeSetValue[nodeId];
            
        }

        os << j.dump();
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::ComputeInference(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "COMPUTE INFERENCE " << std::endl;
#endif

    try
    {
        _inference_engine->makeInference();

        while (!_inference_engine->isDone())
        {
            std::cerr << "Waiting Inference Engine" << std::endl;
        }

        os << "inference done";
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::LoadCSV(const std::string &filename, const std::string &bn_name)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "LOAD CSV " << std::endl;
#endif

    if (_db != nullptr)
    {
        delete _db;
        _db = nullptr;
    }

    try
    {
        // Create database from discretized data CSV
        // 1/ use the initializer to parse all the columns/rows of a CSV file
        learning::DBInitializerFromCSV<> initializer(filename);
        const auto &var_names = initializer.variableNames();
        const std::size_t nb_vars = var_names.size();

        // we create as many translators as there are variables
        learning::DBTranslator4LabelizedVariable<> translator;
        learning::DBTranslatorSet<> translator_set;
        for (std::size_t i = 0; i < nb_vars; ++i)
            translator_set.insertTranslator(translator, i);

        // create a DatabaseTable with these translators. For the moment, the
        // DatabaseTable will be empty, i.e., it will contain no row
        _db = new learning::DatabaseTable<>(translator_set);
        _db->setVariableNames(initializer.variableNames());

        // use the DBInitializerFromCSV to fill the rows:
        initializer.fillDatabase(*_db);

        // now, the database contains all the content of the CSV file
        // Learn from database
        learning::BNLearner<T> bnl(*_db);
        // Update name
        _bn_instance = bnl.learnBN();
        _bn_instance.setProperty("name", bn_name);
        name = bn_name;

        os << "load CSV done";
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}


template <class T>
std::string JSBayesNet<T>::GetAllLists(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "GET ALL LIST" << std::endl;
#endif

    try
    {
        json j = {};

        for (auto nodeId : _bn_instance.nodes())
        {
            j["NodesList"][nodeId]["id"] = nodeId;
            j["NodesList"][nodeId]["name"] = _bn_instance.variable(nodeId).name();
            j["NodesList"][nodeId]["group"] =group.getWithDefault(nodeId, 0);
            int i = 0;
            for (auto label : _bn_instance.variable(nodeId).labels())
            {
                j["NodesList"][nodeId]["labels"][i] = label;
                i++;
            }  

            j["NodesPosList"][nodeId]["x"] = posXT.getWithDefault(nodeId, 0);
            j["NodesPosList"][nodeId]["y"] = posYT.getWithDefault(nodeId, 0);
            j["NodesInfPosList"][nodeId]["x"] = posXTInf.getWithDefault(nodeId, 0);
            j["NodesInfPosList"][nodeId]["y"] = posYTInf.getWithDefault(nodeId, 0);

        }

        int i = 0;
        for (const auto arc : _bn_instance.dag().arcs())
        {
            j["ArcsList"][i]["tail"] = arc.tail();
            j["ArcsList"][i]["head"] = arc.head();
            i++;
        }

        os << j.dump();
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
        os << "Error:" << e.what() << std::endl;
    }

    return os.str();
}

template <class T>
std::string JSBayesNet<T>::GetName(void)
{

    std::ostringstream os;

#ifdef NDEBUG
    std::cerr << "GET NAME" << std::endl;
#endif
    try
    {
        os << name;
    }
    catch (Exception e)
    {
        std::cerr << e.what() << std::endl;
    }

    return os.str();
}


////////////////////////////////////////////////////////////////////
// utilities
////////////////////////////////////////////////////////////////////
std::string _rgb(unsigned int r, unsigned int g, unsigned int b)
{
    std::ostringstream os;
    os << "rgb(" << r << "," << g << "," << b << ")";
    return os.str();
}

std::string _mkCell(double val, short int digits, bool withColour)
{
    std::ostringstream os;
    os << "<td style=\"";
    if (withColour && (val >= 0 && val <= 1))
    {
        os << "background-color:"
           << _rgb((unsigned int)(255 - val * 128), (unsigned int)(127 + val * 128), 100)
           << ";";
    }
    os << "text-align:right;\">" << std::setprecision(digits) << val << "</td>";
    return os.str();
}
