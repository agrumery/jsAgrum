#ifndef _JSBAYESNET_
#define _JSBAYESNET_

#include <agrum/core/debug.h>
#include <agrum/BN/BayesNet.h>
#include <agrum/BN/inference/lazyPropagation.h>

// Learner CSV
#include <agrum/learning/BNLearner.h>
#include <agrum/learning/database/DBInitializerFromCSV.h>
#include <agrum/learning/database/DBTranslator4LabelizedVariable.h>

using namespace gum;

#include <emscripten/bind.h>

template <class T>
class JSBayesNet
{
private:
    std::string name;

    BayesNet<T> _bn_instance;
    LazyPropagation<T> *_inference_engine;
    learning::DatabaseTable<> *_db;

    gum::NodeProperty<int> posXT;
    gum::NodeProperty<int> posYT;
    gum::NodeProperty<int> posXTInf;
    gum::NodeProperty<int> posYTInf;
    gum::NodeProperty<int> group;

public:
    JSBayesNet(const std::string &name);

    ~JSBayesNet();

    std::string AddNode(const std::string &name, const unsigned int nbrmod, const unsigned int posX, const unsigned int posY);
    std::string RemoveNode(const unsigned int varId);
    std::string ChangeNodeName(const unsigned int varId, const std::string &name);
    std::string ChangeLabelsName(const unsigned int varId, const std::string &jsontxt);
    std::string GetNodesList(void);

    std::string AddArc(const unsigned int tailId, const unsigned int headId);
    std::string RemoveArc(const unsigned int tailId, const unsigned int headId);
    std::string GetArcsList(void);

    std::string ToString(void);
    std::string ToDot(void);

    std::string GeneraCTPs(void);
    std::string Cpt(const unsigned int varId);
    std::string AddCpt(const unsigned int varId, const std::string &jsontxt);

    std::string MoveNode(const unsigned int varId, const int posX, const int posY);
    std::string GetPosNode(const unsigned int varId);
    std::string MoveNodeInf(const unsigned int varId, const int posX, const int posY);
    std::string GetPosNodeInf(const unsigned int varId);
    std::string GetAllPosNode(void);

    std::string SetGroupNode(const unsigned int varId, const int group);
    std::string GetGroupNode(const unsigned int varId);

    std::string LoadBN(const std::string &name, const unsigned int type);
    std::string SaveBN(const std::string &name);
    std::string LoadBNPos(const std::string &jsonTxt);
    std::string SaveBNPos(const std::string &name);

    std::string ResetInferenceEngine(void);
    std::string ComputeInference(void);
    std::string Posterior(const unsigned int varId);
    std::string AddEvidence(const unsigned int varId, const unsigned int eviValue);
    std::string ChgEvidence(const unsigned int varId, const unsigned int eviValue);
    std::string EraseEvidence(const unsigned int varId);
    std::string HardEvidence(void);

    std::string LoadCSV(const std::string &filename, const std::string &bn_name);

    std::string GetAllLists(void);

    std::string GetName(void);

private:
    std::string PosteriorAsHTML(const Potential<T> &p);
    std::string PosteriorAsJson(const Potential<T> &p);
};

EMSCRIPTEN_BINDINGS(my_module)
{
    emscripten::class_<JSBayesNet<double>>("BayesNetDouble")
        .constructor<std::string>()
        .function("AddNode", &JSBayesNet<double>::AddNode)
        .function("RemoveNode", &JSBayesNet<double>::RemoveNode)
        .function("GetNodesList", &JSBayesNet<double>::GetNodesList)
        .function("AddArc", &JSBayesNet<double>::AddArc)
        .function("RemoveArc", &JSBayesNet<double>::RemoveArc)
        .function("GetArcsList", &JSBayesNet<double>::GetArcsList)
        .function("ToString", &JSBayesNet<double>::ToString)
        .function("ToDot", &JSBayesNet<double>::ToDot)
        .function("GeneraCTPs", &JSBayesNet<double>::GeneraCTPs)
        .function("Cpt", &JSBayesNet<double>::Cpt)
        .function("AddCpt", &JSBayesNet<double>::AddCpt)
        .function("MoveNode", &JSBayesNet<double>::MoveNode)
        .function("GetPosNode", &JSBayesNet<double>::GetPosNode)
        .function("GetAllPosNode", &JSBayesNet<double>::GetAllPosNode)
        .function("LoadBN", &JSBayesNet<double>::LoadBN)
        .function("SaveBN", &JSBayesNet<double>::SaveBN)
        .function("LoadBNPos", &JSBayesNet<double>::LoadBNPos)
        .function("SaveBNPos", &JSBayesNet<double>::SaveBNPos)
        .function("Posterior", &JSBayesNet<double>::Posterior)
        .function("AddEvidence", &JSBayesNet<double>::AddEvidence)
        .function("ComputeInference", &JSBayesNet<double>::ComputeInference)
        .function("ResetInferenceEngine", &JSBayesNet<double>::ResetInferenceEngine)
        .function("LoadCSV", &JSBayesNet<double>::LoadCSV)
        .function("GetAllLists", &JSBayesNet<double>::GetAllLists)
        .function("ChangeNodeName", &JSBayesNet<double>::ChangeNodeName)
        .function("ChangeLabelsName", &JSBayesNet<double>::ChangeLabelsName)
        .function("ChgEvidence", &JSBayesNet<double>::ChgEvidence)
        .function("EraseEvidence", &JSBayesNet<double>::EraseEvidence)
        .function("HardEvidence", &JSBayesNet<double>::HardEvidence)
        .function("MoveNodeInf", &JSBayesNet<double>::MoveNodeInf)
        .function("GetPosNodeInf", &JSBayesNet<double>::GetPosNodeInf)
        .function("SetGroupNode", &JSBayesNet<double>::SetGroupNode)
        .function("GetGroupNode", &JSBayesNet<double>::GetGroupNode)
        .function("GetName",  &JSBayesNet<double>::GetName)
        ;
}

// Utilities
std::string _rgb(unsigned int r, unsigned int g, unsigned int b);
std::string _mkCell(double val, short int digits, bool withColour);

#endif // _JSBAYESNET_
