JSAGRUM_PATH = ./jsAgrum/
ELECTRON_SRC = ./electron/
SRC_PATH = ${JSAGRUM_PATH}src/
SRC_FILE = ${SRC_PATH}JSBayesNet.cpp
LIB_PATH = ${JSAGRUM_PATH}lib/
LIB_NODE_PATH = ${LIB_PATH}nodejs/
LIB_NAME = libagrum.js
LIB_FILE = ${LIB_PATH}${LIB_NAME}
BC_FILE = ${LIB_PATH}JSBayesNet.bc
DEPLOY_FILE = ./deploy/
ELECTRON_FILE = ./deploy_electron/

DEBUG=-s RUNTIME_LOGGING=1  -s ASSERTIONS=2 -s SAFE_HEAP=1 -O0 --js-opts 0 --llvm-opts 0 -g3
RELEASE=-Os --js-opts 1 --llvm-opts 2

#MODE=${DEBUG}
MODE=${RELEASE}

#EMCC_OPTS= -s ALLOW_MEMORY_GROWTH=1 -s DEMANGLE_SUPPORT=1 -s DISABLE_EXCEPTION_CATCHING=0 -s ASM_JS=2 -s WASM=0 -s ERROR_ON_UNDEFINED_SYMBOLS=0
EMCC_OPTS= -s ALLOW_MEMORY_GROWTH=1 -s DEMANGLE_SUPPORT=1 -s DISABLE_EXCEPTION_CATCHING=0 -s BINARYEN_TRAP_MODE='clamp'
#EMCC_LNK= -s ALIASING_FUNCTION_POINTERS=0 -s RESERVED_FUNCTION_POINTERS=0
EMCC_LNK=

CLEAN_FILE = ${LIB_FILE}* ${LIB_FILE} ${BC_FILE} $(patsubst %.js,%.wasm,${LIB_NAME}) $(patsubst %.js,%.wast,${LIB_NAME})

clean:
	rm -f ${CLEAN_FILE}

phase1: ${LIB_PATH}libagrum.bc
	@echo "\e[33;1m-- /!\\ Pre Builds ${LIB_PATH}${LIB_NAME} (${MODE})\e[0m" 
	@emcc -std=c++14 ${MODE} --bind ${EMCC_OPTS} ${EMCC_LNK} ${LIB_PATH}libagrum.bc -o ${LIB_PATH}${LIB_NAME}

phase2: ${SRC_FILE} ${SRC_PATH}JSBayesNet.h
	@echo "\e[33;1m-- /!\\ Builds ${BC_FILE} (${MODE})\e[0m"
	@emcc -std=c++14 ${MODE} --bind ${EMCC_OPTS} ${EMCC_LNK} -I ${JSAGRUM_PATH}include -o ${BC_FILE} ${SRC_FILE} -c

phase3: ${LIB_PATH}libagrum.bc ${BC_FILE} ${SRC_PATH}js/libagrum_before.js ${SRC_PATH}js/libagrum_after.js
	@echo "\e[33;1m-- /!\\ Builds ${LIB_PATH}${LIB_NAME} (${MODE})\e[0m"
	@emcc -std=c++14 ${MODE} --bind ${EMCC_OPTS} ${EMCC_LNK} -s EXPORTED_FUNCTIONS="[]" --pre-js ${SRC_PATH}js/libagrum_before.js --post-js ${SRC_PATH}js/libagrum_after.js \
	--memory-init-file 0 ${LIB_PATH}libagrum.bc ${BC_FILE} -o ${LIB_PATH}${LIB_NAME}

compile_node_version : ${LIB_PATH}libagrum.bc ${BC_FILE} ${SRC_PATH}js/libagrum_before_node.js ${SRC_PATH}js/libagrum_after.js
	@echo "\e[33;1m-- /!\\ Builds ${LIB_NODE_PATH}${LIB_NAME} (${MODE})\e[0m"
	@mkdir -p ${LIB_NODE_PATH}
	@emcc -std=c++14 ${MODE} --bind ${EMCC_OPTS} ${EMCC_LNK} -s EXPORTED_FUNCTIONS="[]" --pre-js ${SRC_PATH}js/libagrum_before_node.js --post-js ${SRC_PATH}js/libagrum_after.js \
	--memory-init-file 0 ${LIB_PATH}libagrum.bc ${BC_FILE} -o ${LIB_NODE_PATH}${LIB_NAME}

deploy_local : 
	ln -s ../../${SRC_PATH}js/libjsagrum_utils.js ${JSAGRUM_PATH}lib

deploy:
	mkdir -p ${DEPLOY_FILE}${JSAGRUM_PATH}
	mkdir -p ${DEPLOY_FILE}docs
	cp index.html ${DEPLOY_FILE}
	cp style.css ${DEPLOY_FILE}
	cp -r img ${DEPLOY_FILE}
	cp -r apps ${DEPLOY_FILE}
	cp -r BNrepository ${DEPLOY_FILE}
	cp -Lr ${JSAGRUM_PATH}lib ${DEPLOY_FILE}${JSAGRUM_PATH}
	cp ${SRC_PATH}js/libjsagrum_utils.js ${DEPLOY_FILE}${JSAGRUM_PATH}lib
	cp -r docs ${DEPLOY_FILE}
	. ${DEPLOY_FILE}docs/regenerate_docs.sh 

deploy_electron:
	mkdir -p ${ELECTRON_FILE}${JSAGRUM_PATH}
	cp index.html ${ELECTRON_FILE}
	cp style.css ${ELECTRON_FILE}
	cp -r img ${ELECTRON_FILE}
	cp -r apps ${ELECTRON_FILE}
	cp -Lr ${JSAGRUM_PATH}lib ${ELECTRON_FILE}${JSAGRUM_PATH}
	cp ${SRC_PATH}js/libjsagrum_utils.js ${ELECTRON_FILE}${JSAGRUM_PATH}lib
	cp ${ELECTRON_SRC}src/* ${ELECTRON_FILE}
	cp ${ELECTRON_SRC}img/* ${ELECTRON_FILE}

all: phase2 phase3


