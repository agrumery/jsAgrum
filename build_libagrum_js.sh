#!/bin/bash

# version
AGRUM=0.17.1

# functions
function error_msg() {
    if [ $# -gt 0 ]; then 
	echo -e "\e[31;1m-- $1\e[0m"
    fi
}

function warn_msg() {
    if [ $# -gt 0 ]; then 
	echo -e "\e[33;1m-- /!\\ $1\e[0m"
    fi
}

function normal_msg() {
    if [ $# -gt 0 ]; then 
	echo -e "\e[32;1m-- $1\e[0m"
    fi
}

function isOK() {
    if [ $? -gt 0 ]; then
    error_msg "FAILURE"
    else
    normal_msg "DONE"
    fi
}

# sourcing emscripten
normal_msg "Sourcing Emscripten environment..."
if [ -e emsdk_env.sh ]; then
. emsdk_env.sh
elif [ -e /usr/local/emsdk_portable/emsdk_env.sh ]; then
. /usr/local/emsdk_portable/emsdk_env.sh
elif [ -e /usr/local/emsdk-portable/emsdk_env.sh ]; then
. /usr/local/emsdk-portable/emsdk_env.sh
elif [ -e ~/emsdk/emsdk_env.sh ]; then
. ~/emsdk/emsdk_env.sh
elif [ 'emcc -h' ]; then
normal_msg "emcc found"
else
error_msg "emsdk_env.sh not found ! Aborting..."
normal_msg "Download and install emscripten at https://emscripten.org/docs/getting_started/downloads.html"
exit -1
fi

# aGrum is not compiled
warn_msg "Checks if aGrum is compiled... (${PWD})"
if [ ! -f jsAgrum/lib/libagrum.bc ] ; then

# build dir

mkdir -p archives ; cd archives
wget -c https://gitlab.com/agrumery/aGrUM/-/archive/${AGRUM}/aGrUM-${AGRUM}.zip \
&& unzip aGrUM-${AGRUM}.zip \
&& ln -s aGrUM-${AGRUM} agrum \
&& cd aGrUM-${AGRUM}

# build dir
mkdir -p build ; cd build
emcmake cmake -G"Unix Makefiles" \
-DBUILD_ALL=OFF \
-DBUILD_BASE=ON \
-DBUILD_BN=ON \
-DBUILD_CN=ON \
-DBUILD_FMDP=ON \
-DBUILD_ID=ON \
-DBUILD_JAVA=OFF \
-DBUILD_LEARNING=ON \
-DBUILD_PRM=ON \
-DBUILD_PYTHON=OFF \
-DBUILD_SHARED_LIBS=OFF \
-DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX=../../../jsAgrum \
-DEMSCRIPTEN=ON \
-DEMSCRIPTEN_FORCE_COMPILERS=ON \
-DEMSCRIPTEN_GENERATE_BITCODE_STATIC_LIBRARIES=ON \
-DFOR_PYTHON3=OFF \
-DUSE_NANODBC=OFF \
-DUSE_SWIG=OFF \
..

emmake make -j $(nproc) && emmake make install

cd ../../..
else
normal_msg "Agrum already compiled"
fi

isOK

# compilation
warn_msg "Compiles program... (${PWD})"

DEBUG="-s RUNTIME_LOGGING=1  -s ASSERTIONS=2 -s SAFE_HEAP=1 -O0 --js-opts 0 --llvm-opts 0 -g3 "
RELEASE="-Os --js-opts 1 --llvm-opts 2 "
#MODE=${DEBUG}
MODE=${RELEASE}
#EMCC_OPTS="-s ALLOW_MEMORY_GROWTH=1 -s DEMANGLE_SUPPORT=1 -s DISABLE_EXCEPTION_CATCHING=0 -s ASM_JS=2 -s WASM=0 -s ERROR_ON_UNDEFINED_SYMBOLS=0"
EMCC_OPTS="-s ALLOW_MEMORY_GROWTH=1 -s DEMANGLE_SUPPORT=1 -s DISABLE_EXCEPTION_CATCHING=0 -s BINARYEN_TRAP_MODE='clamp' "
#EMCC_LNK="-s ALIASING_FUNCTION_POINTERS=0 -s RESERVED_FUNCTION_POINTERS=0 "
EMCC_LNK=""
rm -f jsAgrum/lib/libagrum.js* jsAgrum/lib/JSBayesNet.bc jsAgrum/lib/libagrum.js

warn_msg "Builds ./jsAgrum/lib/libagrum.js (${MODE})"
emcc -std=c++14 ${MODE} --bind ${EMCC_OPTS} ${EMCC_LNK} ./jsAgrum/lib/libagrum.bc -o ./jsAgrum/lib/libagrum.js
isOK

warn_msg "Builds main.bc (${MODE})"
emcc -std=c++14 ${MODE} --bind ${EMCC_OPTS} ${EMCC_LNK} -I jsAgrum/include -o ./jsAgrum/lib/JSBayesNet.bc ./jsAgrum/src/JSBayesNet.cpp -c
isOK

warn_msg "Builds ./lib/libagrum.js (${MODE})"
emcc -std=c++14 ${MODE} --bind ${EMCC_OPTS} ${EMCC_LNK} -s EXPORTED_FUNCTIONS="[]" --pre-js jsAgrum/src/js/libagrum_before.js --post-js jsAgrum/src/js/libagrum_after.js \
--memory-init-file 0 \
./jsAgrum/lib/libagrum.bc ./jsAgrum/lib/JSBayesNet.bc -o ./jsAgrum/lib/libagrum.js
isOK
