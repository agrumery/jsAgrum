/** @module Graph Utilities */

// Global variables
/**  keeps the graph handler */
var graph;

/**  keeps the del key handler */
var keyHandler = null;

/** keeps node chart array */
var chartArray = {};

/** keeps the BN graph Edges ([].tail, [].head) */
var mxArcsList;
/** keeps the BN graph Nodes ([].id, [].name, [].labels = [0, 1]) */
var mxNodesList;
/** keeps the BN graph Positions ([].x, [].y) */
var mxPosList;
/** keeps the BN inference graph Positions ([].x, [].y) */
var mxPosInfList;
/** keeps the BN graph Hard Evidences ([].id, [].name, [].labelIdx) */
var mxHardEvidence;

/** keeps the current posterior status of node ([][i].value, [][i].name) */
var mxPosteriorList = {};

/** keeps the arc source */
var arcSource;

/** keeps cells selection */
var selection;
var lastSelection = null;

/** keeps the selected node Id */
var nodeSelectionId;

/** disables edge connection callback during redraw */
var disableConnexion = 0;

/** keeps if we are generating the print page */
var printingState = 0;

/**
 *  Converts cell element into nodeId
 *  @param {object} cell Cell object.
 *  @returns {integer} nodeId.
 */
var convertCellToId = function(cell) {
    var ret = "";
    mxNodesList.forEach(el => {
        if (el && cell && cell.value == el.name) {
            ret = el.id;
        }
    });
    return ret;
}

/**
 *  Converts name element into nodeId
 *  @param {string} name Cell name.
 *  @returns {integer} nodeId.
 */
var convertNameToId = function(name) {
    var ret = "";
    mxNodesList.forEach(el => {
        if (el && name && name == el.name) {
            ret = el.id;
        }
    });
    return ret;
}

/**
 * return the cell object with a given name
 * @param {string} name Name of the cell to find.
 */
var recoverCell = function(name) {
    var listCells = graph.getModel().cells;
    var nbCell = graph.getModel().nextId;

    for (let idx = 0; idx < nbCell; idx++) {
        var cell = listCells[idx];
        if (cell.value == name) {
            return cell;
        }
    }
    return null;
}

/**
 *  handles potential errors when removing edge
 *  @param {object} edge Edge object.
 */
var customRemoveCell = function(edge) {
    graph.getModel().beginUpdate();
    try {
        graph.getModel().remove(edge);
    } finally {
        graph.getModel().endUpdate();
    }
}

/**
 * changes the style of a cell according to the group
 * @param {object} cell Cell to change.
 * @param {integer} group New group to apply.
 *  0 : standard (blue)
 *  1 : red
 *  2 : green
 *  3 : purple
 *  4 : yellow
 */
var changeStyleGroupCell = function(cell, group) {

    if (group == 1) {
        modifyStyleCell(cell, "strokeWidth", 2);
        modifyStyleCell(cell, "strokeColor", "#FF5145");
        if (graphMode == 0) {
            modifyStyleCell(cell, "fillColor", "#FF8563");
        }
    } else if (group == 2) {
        modifyStyleCell(cell, "strokeWidth", 2);
        modifyStyleCell(cell, "strokeColor", "#49D11B");
        if (graphMode == 0) {
            modifyStyleCell(cell, "fillColor", "#53F01F");
        }
    } else if (group == 3) {
        modifyStyleCell(cell, "strokeWidth", 2);
        modifyStyleCell(cell, "strokeColor", "#C138E8");
        if (graphMode == 0) {
            modifyStyleCell(cell, "fillColor", "#C176E8");
        }
    } else if (group == 4) {
        modifyStyleCell(cell, "strokeWidth", 2);
        modifyStyleCell(cell, "strokeColor", "#E6BA2C");
        if (graphMode == 0)
            modifyStyleCell(cell, "fillColor", "#FFCF3B");
    } else {
        modifyStyleCell(cell, "strokeWidth", 2);
        modifyStyleCell(cell, "strokeColor", "#7EB1E0");
        if (graphMode == 0) {
            modifyStyleCell(cell, "fillColor", "#8FC9FF");
        }
    }
}

/**
 * changes the group of the current selection
 * @param {integer} group group number. 
 */
var changeGroupCellSelection = function(group) {
    // if a selection is done
    if (!graph.getSelectionModel().isEmpty()) {
        if (selection) {
            for (let index = 0; index < selection.length; index++) {
                if (selection[index] && selection[index].vertex) {
                    setGroupNode(convertCellToId(selection[index]), group);
                    changeStyleGroupCell(selection[index], group);
                    mxNodesList[convertCellToId(selection[index])]["group"] = group;
                }
            }
        }
    } else {
        //if no selection is done
        if (mxNodesList) {
            let newSelection = [];
            mxNodesList.forEach(el => {
                if (el && (el.group != null) && el.group == group) {
                    let cell = recoverCell(el.name);
                    if (cell) {
                        newSelection.push(cell);
                    }
                }
            });
            graph.selectCellsForEvent(newSelection);
        }
    }
}

var changeNodeModeNumber = function(nodeId, name, nbMode) {

    let arcList = [];
    var x = mxPosList[nodeId].x;
    var y = mxPosList[nodeId].y;
    var infX = mxPosInfList[nodeId].x;
    var infY = mxPosInfList[nodeId].y;
    var group = mxNodesList[nodeId].group;

    if (mxArcsList) {
        mxArcsList.forEach(el => {
            if ((el.head == nodeId) || (el.tail == nodeId)) {
                arcList.push(el);
            }
        });
    }

    removeNode(nodeId).then(
        (res) => {
            return addNode(name, nbMode, x, y);
        }
    ).then(
        (res) => {
            for (let index = 0; index < arcList.length; index++) {
                const arc = arcList[index];
                addArc(arc.tail, arc.head);
            }
            return refreshGraph();
        }
    ).then(
        (res) => {
            allListRefresh(res);
        }
    ).then(
        (res) => {
            moveNodeInf(nodeId, infX, infY);
            setGroupNode(nodeId, group);
            let cell = recoverCell(mxNodesList[nodeId].name);
            changeStyleGroupCell(cell, group);
            mxNodesList[nodeId]["group"] = group;
        }
    ).catch(
        (err) => {
            console.log('Got error: ' + err);
            openErrorMsg(err);
        }
    );

}

/**
 * Move Selected cells in a given direction
 * @param {string} dir movement direction. 
 */
var moveGroupCellSelection = function(dir) {
    if (!graph.getSelectionModel().isEmpty()) {
        if (selection) {

            let dx = 0;
            let dy = 0;

            if (dir == "up") {
                dy = -10;
            } else if (dir == "bottom") {
                dy = 10;
            } else if (dir == "right") {
                dx = 10;
            } else if (dir == "left") {
                dx = -10;
            }

            graph.moveCells(selection, dx, dy);
        }
    }
}


/**
 *  sends a message to recover the new posterior for each created chart
 */
var refreshChartGraph = function() {

    Object.keys(chartArray).forEach(function(key) {
        posterior(convertNameToId(key)).then(
            (data) => {
                try {
                    let j = JSON.parse(data.string);
                    if (chartArray[j.node.name]) {
                        let recvData = [];
                        for (let index = 0; index < j.data.length; index++) {
                            recvData[index] = {};
                            recvData[index]["value"] = j.data[index];
                            recvData[index]["name"] = j.node.labels[index];
                        }
                        mxPosteriorList[j.node.name] = recvData;
                        updateLabelChart(j.node.name);
                    }
                } catch (error) {
                    console.error(error);
                }
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    })
}

/**
 *  Checks if the hard evidence is already apply (by node name)
 *  @param {string} name Node name.
 *  @returns {boolean} true?.
 */
var isInHardEvidenceList = function(name) {

    if (mxHardEvidence) {
        for (let idx = 0; idx < mxHardEvidence.length; idx++) {
            if (mxHardEvidence[idx] && mxHardEvidence[idx].name == name) {
                return true;
            }
        }
    }
    return false;
}

/**
 * Modify a tag value of a cell
 * @param {Object} cell Cell to change.
 * @param {string} tag selected Tag.
 * @param {string} value New Value of the tag.
 */
var modifyStyleCell = function(cell, tag, value) {

    let style = graph.getModel().getStyle(cell);
    let modif_ok = 0;
    let new_style = "";

    style.split(";").forEach(el => {
        let svalue = el.split("=");
        if (svalue[0] == tag) {
            svalue[1] = value;
            modif_ok = 1;
        }
        new_style += svalue[0] + "=" + svalue[1] + ";";
    })

    if (modif_ok == 0) {
        new_style = style + tag + "=" + value + ";";
    }

    graph.getModel().setStyle(cell, new_style);
}

/**
 *  Updates the color node if an evidence is set
 */
var refreshColorNode = function() {

    var listCells = graph.getModel().cells;
    var nbCell = graph.getModel().nextId;

    for (let idx = 0; idx < nbCell; idx++) {
        var cell = listCells[idx];
        if (cell.value && cell.vertex) {
            if (isInHardEvidenceList(cell.value)) {
                modifyStyleCell(cell, "strokeWidth", 4);
            } else {
                modifyStyleCell(cell, "strokeWidth", 2);
            }
        }
    }
}

/**
 *  Updates the color node depending of the group
 */
var refreshGroupColorNode = function() {

    var listCells = graph.getModel().cells;
    var nbCell = graph.getModel().nextId;

    for (let idx = 0; idx < nbCell; idx++) {
        var cell = listCells[idx];
        if (mxNodesList && cell.value && cell.vertex) {
            var id = convertCellToId(cell);
            if (mxNodesList[id].group) {
                changeStyleGroupCell(cell, mxNodesList[id].group);
            } else {
                changeStyleGroupCell(cell, 0);
            }
        }
    }

    if (graphMode == 1) {
        refreshColorNode();
    }
}

/**
 *  Updates node menu in graphical mode
 */
var refreshNodeMenu = function() {

    if (mxNodesList && mxNodesList[nodeSelectionId]) {
        // identify node
        document.getElementById("node_menu_title").textContent = "Node : " + mxNodesList[nodeSelectionId].name;
        document.getElementById("nodeName").value = mxNodesList[nodeSelectionId].name;
        // update Labels
        let labelsNb = Object.keys(mxNodesList[nodeSelectionId].labels).length;
        document.getElementById("nbrm").value = labelsNb;
        let labelsDiv = document.getElementById("nodeLabelList");
        labelsDiv.innerHTML = "";
        for (i = 0; i < labelsNb; i++) {
            let inputText = document.createElement("INPUT");
            inputText.classList = "customInput nodeInput";
            inputText.type = "text";
            inputText.name = "label-" + i;
            inputText.id = "label-" + i;
            inputText.value = mxNodesList[nodeSelectionId].labels[i];
            labelsDiv.appendChild(inputText);
        }
        // Call for the Cpt of the node
        cpt(nodeSelectionId).then(
            (data) => cptRefresh(data)
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
        // Group status
        let groupText = document.getElementById("numg");
        if (mxNodesList[nodeSelectionId].group) {
            groupText.value = mxNodesList[nodeSelectionId].group;
        } else {
            groupText.value = 0;
        }
    }

}

/**
 *  Updates node menu in inference mode
 */
var refreshInfNodeMenu = function() {

    if (mxNodesList && mxNodesList[nodeSelectionId]) {
        // identify node
        document.getElementById("inf_node_menu_title").textContent = "Node : " + mxNodesList[nodeSelectionId].name;
        // update Labels
        let labelsNb = Object.keys(mxNodesList[nodeSelectionId].labels).length;

        let labelsDiv = document.getElementById("infNodeLabelList");
        labelsDiv.innerHTML = "";

        let checkedIdx = -1;
        if (mxHardEvidence && mxHardEvidence[nodeSelectionId]) {
            checkedIdx = mxHardEvidence[nodeSelectionId].value;
        }

        for (i = 0; i < labelsNb; i++) {
            let divinput = document.createElement("div");
            let inputText = document.createElement("INPUT");
            inputText.type = "radio";
            inputText.name = "infLabel-" + mxNodesList[nodeSelectionId].name;
            inputText.id = mxNodesList[nodeSelectionId].labels[i] + "-" + mxNodesList[nodeSelectionId].name;
            inputText.value = mxNodesList[nodeSelectionId].labels[i];
            inputText.nodeId = nodeSelectionId;
            inputText.labelId = i;
            inputText.addEventListener("change", function() {
                addEvidence(this.nodeId, this.labelId).then(
                    (res) => {
                        return hardEvidence();
                    }
                ).then(
                    (data) => {
                        hardEvidenceListRefresh(data);
                        refreshChartGraph();
                    }
                ).catch(
                    (err) => {
                        console.log('Got error: ' + err);
                        openErrorMsg(err);
                    }
                );
            });
            if (checkedIdx == i) { inputText.checked = 'true'; }
            divinput.appendChild(inputText);
            let inputLabel = document.createElement("LABEL");
            inputLabel.for = mxNodesList[nodeSelectionId].labels[i] + "-" + mxNodesList[nodeSelectionId].name;
            inputLabel.textContent = mxNodesList[nodeSelectionId].labels[i];
            divinput.appendChild(inputLabel);
            labelsDiv.appendChild(divinput);
        }
        let divinput = document.createElement("div");
        let inputText = document.createElement("INPUT");
        inputText.type = "radio";
        inputText.name = "infLabel-" + mxNodesList[nodeSelectionId].name;
        inputText.id = 'none-' + mxNodesList[nodeSelectionId].name;
        inputText.value = 'none';
        inputText.nodeId = nodeSelectionId;
        inputText.addEventListener("change", function() {
            eraseEvidence(this.nodeId).then(
                (res) => {
                    return hardEvidence();
                }
            ).then(
                (data) => {
                    hardEvidenceListRefresh(data);
                    refreshChartGraph();
                }
            ).catch(
                (err) => {
                    console.log('Got error: ' + err);
                    openErrorMsg(err);
                }
            );
        });
        if (checkedIdx == -1) { inputText.checked = 'true'; }
        divinput.appendChild(inputText);
        let inputLabel = document.createElement("LABEL");
        inputLabel.for = 'none-' + mxNodesList[nodeSelectionId].name;
        inputLabel.textContent = 'none';
        divinput.appendChild(inputLabel);
        labelsDiv.appendChild(divinput);

    }

}

/**
 *  Apply a new layout, recovers position of each node, sends the position to BN and redraw the graph
 *  @param {string} layoutType Layout type wanted.
 */
var applyNewLayout = function(layoutType) {

    // define layout
    var layoutG;

    switch (layoutType) {
        case "Circle":
            layoutG = new mxCircleLayout(graph);
            break;
        case "Composite":
            layoutG = new mxCompositeLayout(graph);
            break;
        case "CompactTree":
            layoutG = new mxCompactTreeLayout(graph);
            break;
        case "FastOrganic":
            layoutG = new mxFastOrganicLayout(graph);
            //layoutG.forceConstant = 150;
            //layoutG.minDistanceLimit = 5;
            break;
        case "ParallelEdge":
            layoutG = new mxParallelEdgeLayout(graph);
            break;
        case "Partition":
            layoutG = new mxPartitionLayout(graph);
            break;
        case "Stack":
            layoutG = new mxStackLayout(graph);
            break;
        case "Hierarchical":
        default:
            mxHierarchicalLayout.prototype.edgeStyle = mxHierarchicalEdgeStyle.STRAIGHT;
            layoutG = new mxHierarchicalLayout(graph);
            break;
    }

    layoutG.execute(graph.getDefaultParent());

    // update position inside the BN lib
    let cells = graph.getModel().cells
    if (cells) {
        for (var i = 0; i < Object.keys(cells).length; i++) {
            let cell = cells[i];
            if (cell.vertex) {
                let x = cell.geometry.x;
                let y = cell.geometry.y;
                if (graphMode == 1) {
                    moveNodeInf(convertCellToId(cell), x, y);
                } else {
                    moveNode(convertCellToId(cell), x, y);
                }

            }
        }
    }

    // refresh graph
    refreshGraph().then(
        (res) => {
            allListRefresh(res);
        }
    ).catch(
        (err) => {
            console.log('Got error: ' + err);
            openErrorMsg(err);
        }
    );

}

/**
 *  Redraws the graph in graphical mode
 *  @param {boolean} saveState true if you want to recover the graph parameter
 */
var redrawGraph = function(saveState) {

        disableConnexion = 1;

        let view, vScale, vBounds, vTranlate;

        if (saveState) {
            // Keep the view status to recover the previous view
            view = graph.getView();
            vScale = view.getScale();
            vBounds = view.getGraphBounds();
            vTranlate = view.getTranslate();
        }

        // Reset graph
        graph.getModel().setRoot(graph.getModel().createRoot());

        // Gets the default parent for inserting new cells. This
        // is normally the first child of the root (ie. layer 0).
        var parent = graph.getDefaultParent();

        // Adds cells to the model in a single step
        graph.getModel().beginUpdate();
        try {
            var Nodeshash = {};
            var no_position_mode = 0;

            if (mxNodesList && (mxNodesList.length == mxPosList.length)) {

                // Adding node
                mxNodesList.forEach(el => {
                    if (el) {
                        let x;
                        let y;
                        if (mxPosList[el.id] == null ||
                            (mxPosList[el.id].x == 0 && mxPosList[el.id].y == 0)) {
                            no_position_mode++;
                        } else {
                            x = mxPosList[el.id].x;
                            y = mxPosList[el.id].y;
                        }
                        let width = 80;
                        let height = 30;
                        let style = 'shape=ellipse;rounded=0;whiteSpace=wrap;html=1;autosize=1;resizable=0;';
                        var v = graph.insertVertex(parent, null, el.name, x, y, width, height, style);
                        Nodeshash[el.id] = v;

                        var preferred = graph.getPreferredSizeForCell(v);
                        var current = v.getGeometry();
                        current.width = preferred.width + 20;
                        current.height = preferred.height > height ? (preferred.height + 20) : height;

                    }
                });
            }

            if (mxArcsList) {
                // adding edge
                mxArcsList.forEach(el => {
                    graph.insertEdge(parent, null, '', Nodeshash[el.tail], Nodeshash[el.head]);
                });
            }

        } finally {
            // Updates the display
            graph.getModel().endUpdate();
            disableConnexion = 0;
        }

        // Avoid the case where a node is in 0,0
        if (no_position_mode > 2) {
            applyNewLayout(null);
        }

        if (saveState) {
            view = graph.getView();
            view.setScale(vScale);
            view.setGraphBounds(vBounds);
            view.getTranslate(vTranlate);
            view.refresh();
        }

        refreshGroupColorNode();

    }
    /**
     * Generate the ID for the chart div
     * @param {String} name name of the node
     */
var computeIdChart = function(name) {

    return (printingState) ? "chartDiv" + name + "p" : "chartDiv" + name;
}

/**
 *  Redraws the graph in inference mode
 *  @param {boolean} saveState true if you want to recover the graph parameter
 */
var redrawInfGraph = function(saveState) {

    disableConnexion = 1;

    let view, vScale, vBounds, vTranlate;

    if (saveState) {
        // Keep the view status to recover the previous view
        view = graph.getView();
        vScale = view.getScale();
        vBounds = view.getGraphBounds();
        vTranlate = view.getTranslate();
    }

    // Returns canvas with dynamic chart for vertex labels
    var graphConvertValueToString = graph.convertValueToString;
    graph.convertValueToString = function(cell) {

        if (this.model.isVertex(cell) && graph.isCellCollapsed(cell)) {

            var idName = computeIdChart(cell.value);
            var node = document.getElementById(idName);

            if (!printingState) {
                if (!node) {
                    node = document.createElement('div');
                    node.setAttribute('id', idName);
                    node.setAttribute('width', cell.geometry.width);
                    node.setAttribute('height', cell.geometry.height);
                    document.body.appendChild(node);
                    chartArray[cell.value] = node;
                } else {
                    let selectionId = "#" + idName + " svg";
                    d3.select(selectionId).remove();
                    chartArray[cell.value].setAttribute("width", cell.geometry.width);
                    chartArray[cell.value].setAttribute("height", cell.geometry.height);
                }

            } else {

                if (node) {
                    d3.select(idName).remove();
                }
                node = document.createElement('div');
                node.setAttribute('id', idName);
                node.setAttribute('width', cell.geometry.width);
                node.setAttribute('height', cell.geometry.height);
                document.body.appendChild(node);
                chartArray[cell.value] = node;
            }

            if (mxPosteriorList && mxPosteriorList[cell.value]) {
                updateLabelChart(cell.value);
            } else {
                // send message to update the data
                posterior(convertCellToId(cell)).then(
                    (data) => {
                        try {
                            let j = JSON.parse(data.string);
                            if (chartArray[j.node.name]) {
                                let recvData = [];
                                for (let index = 0; index < j.data.length; index++) {
                                    recvData[index] = {};
                                    recvData[index]["value"] = j.data[index];
                                    recvData[index]["name"] = j.node.labels[index];
                                }
                                mxPosteriorList[j.node.name] = recvData;
                                updateLabelChart(j.node.name);
                            }
                        } catch (error) {
                            console.error(error);
                        }
                    }
                ).catch(
                    (err) => {
                        console.log('Got error: ' + err);
                        openErrorMsg(err);
                    }
                );
            }

            return node;
        } else if (this.model.isVertex(cell) && !graph.isCellCollapsed(cell)) {

            return '<table style="overflow:hidden;" width="100%" height="100%" border="0" cellpadding="4" style="height:100%;">' +
                '<tr><th>' + cell.value + '</th></tr>' +
                '</table>';
        }

        return graphConvertValueToString.apply(this, arguments);
    };

    // Reset graph
    graph.getModel().setRoot(graph.getModel().createRoot());

    // Gets the default parent for inserting new cells. This
    // is normally the first child of the root (ie. layer 0).
    var parent = graph.getDefaultParent();

    // Adds cells to the model in a single step
    graph.getModel().beginUpdate();
    try {
        var Nodeshash = {};
        var no_position_mode = 0;

        if (mxNodesList && (mxNodesList.length == mxPosInfList.length)) {

            //adding node
            mxNodesList.forEach(el => {
                if (el) {
                    let x;
                    let y;
                    if (mxPosInfList[el.id] == null ||
                        (mxPosInfList[el.id].x == 0 && mxPosInfList[el.id].y == 0)) {
                        no_position_mode++;
                    } else {
                        x = mxPosInfList[el.id].x;
                        y = mxPosInfList[el.id].y;
                    }
                    let width = 80;
                    let height = 30;
                    let style = 'overflow=fill;fillColor=#FFFFFF;fontColor=#000000;resizable=1;minWidth=150;minHeight=120;'
                    var v = graph.insertVertex(parent, null, el.name, x, y, width, height, style);
                    Nodeshash[el.id] = v;

                    v.geometry.alternateBounds = new mxRectangle(0, 0, 150, 120);

                    var preferred = graph.getPreferredSizeForCell(v);
                    var current = v.getGeometry();
                    current.width = preferred.width + 20;
                    current.height = preferred.height > height ? (preferred.height + 20) : height;


                }
            });
        }

        if (mxArcsList) {
            // adding edge
            mxArcsList.forEach(el => {
                graph.insertEdge(parent, null, '', Nodeshash[el.tail], Nodeshash[el.head]);
            });
        }

    } finally {
        // Updates the display
        graph.getModel().endUpdate();
        disableConnexion = 0;
    }

    // Avoid the case where a node is in 0,0
    if (no_position_mode > 2) {
        applyNewLayout(null);
    }

    if (saveState) {
        view = graph.getView();
        view.setScale(vScale);
        view.setGraphBounds(vBounds);
        view.getTranslate(vTranlate);
        view.refresh();
    }

    refreshGroupColorNode();

}

/**
 *  Defines all option and creates the canvas for the graphical graph
 *  @param {object} container Container Object around the graph.
 */
var graphToMxgraph = function(container) {

    container.innerHTML = "";

    // Checks if the browser is supported
    if (!mxClient.isBrowserSupported()) {
        // Displays an error message if the browser is not supported.
        mxUtils.error('Browser is not supported!', 200, false);
    } else {
        // Disables the built-in context menu
        mxEvent.disableContextMenu(container);

        // Changes some default colors
        mxConstants.HANDLE_FILLCOLOR = 'none';
        mxConstants.HANDLE_STROKECOLOR = 'none';
        mxConstants.VERTEX_SELECTION_COLOR = '#FFB83C';
        mxConstants.EDGE_SELECTION_COLOR = 'none';

        // Creates the graph inside the given container
        graph = new mxGraph(container);
        graph.zoomFactor = 1.05;

        // Enables rubberband selection
        new mxRubberband(graph);

        // Enables the moving canvas with dragging with right click 
        graph.setPanning(true);

        // Changes de perimeter from rectangle to ellipse
        var style = graph.getStylesheet().getDefaultVertexStyle();
        style[mxConstants.STYLE_PERIMETER] = mxPerimeter.EllipsePerimeter;

        redrawGraph(0);

        graph.setAllowDanglingEdges(false);
        graph.setDisconnectOnMove(false);
        graph.setCellsEditable(false);

        var graphCellsMoved = graph.cellsMoved;
        graph.cellsMoved = function(cells, dx, dy, disconnect, constrain, extend) {
            //console.log(" (" + dx + "," + dy + ") " + disconnect + " " + constrain + " "+ extend);

            cells.forEach(cell => {
                //console.log(cell);
                if (cell.vertex) {
                    var nodeId = convertCellToId(cell);
                    var geo = cell.getGeometry();
                    if (nodeId != undefined) {
                        moveNode(nodeId, geo.x + dx, geo.y + dy)
                    }
                    //console.log( cell.value + " " + nodeId + " : " + (geo.x+dx) + " "+ (geo.y+dy));
                }

                if (cell.edge && disconnect) {
                    if (cell.source && cell.target) {
                        console.log("remove arc " + cell.source.value + ">" + cell.target.value);
                        removeArc(convertCellToId(cell.source), convertCellToId(cell.target)).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );
                    }
                }

            });

            graphCellsMoved.apply(this, arguments);
        };

        var graphCellConnected = graph.cellConnected;
        graph.cellConnected = function(edge, terminal, source, constraint) {

            if (!disableConnexion) {
                if (terminal == null) {
                    if (source) {
                        if (edge.target) {
                            if (edge.source) {
                                console.log("remove arc : " + edge.source.value + "->" + edge.target.value);
                                removeArc(convertCellToId(edge.source), convertCellToId(edge.target)).then(
                                    (res) => {
                                        return refreshGraph();
                                    }
                                ).then(
                                    (res) => {
                                        allListRefresh(res);
                                    }
                                ).catch(
                                    (err) => {
                                        console.log('Got error: ' + err);
                                        openErrorMsg(err);
                                    }
                                );
                            }
                            console.log("? ->" + edge.target.value);
                        } else {
                            console.log("? -> ?");
                        }

                    } else {
                        if (edge.source) {
                            if (edge.target) {
                                console.log("remove arc : " + edge.source.value + "->" + edge.target.value);
                                removeArc(convertCellToId(edge.source), convertCellToId(edge.target)).then(
                                    (res) => {
                                        return refreshGraph();
                                    }
                                ).then(
                                    (res) => {
                                        allListRefresh(res);
                                    }
                                ).catch(
                                    (err) => {
                                        console.log('Got error: ' + err);
                                        openErrorMsg(err);
                                    }
                                );
                            }
                            console.log(edge.source.value + " -> ?");
                        } else {
                            console.log("? -> ?");
                        }

                    }
                } else {
                    if (source) {
                        if (edge.target) {
                            if (edge.source) {
                                removeArc(convertCellToId(edge.source), convertCellToId(edge.target)).then(
                                    (res) => {
                                        return refreshGraph();
                                    }
                                ).then(
                                    (res) => {
                                        allListRefresh(res);
                                    }
                                ).catch(
                                    (err) => {
                                        console.log('Got error: ' + err);
                                        openErrorMsg(err);
                                    }
                                );
                                console.log("sup arc : " + edge.source.value + "->" + edge.target.value);
                            }
                            console.log("add arc :" + terminal.value + "->" + edge.target.value);
                            addArc(convertCellToId(terminal), convertCellToId(edge.target)).then(
                                (res) => {
                                    return refreshGraph();
                                }
                            ).then(
                                (res) => {
                                    allListRefresh(res);
                                }
                            ).catch(
                                (err) => {
                                    console.log('Got error: ' + err);
                                    openErrorMsg(err);
                                }
                            );
                        } else {
                            console.log(terminal.value + "-> ?");
                        }

                    } else {
                        if (edge.source) {
                            if (edge.target) {
                                removeArc(convertCellToId(edge.source), convertCellToId(edge.target)).then(
                                    (res) => {
                                        return refreshGraph();
                                    }
                                ).then(
                                    (res) => {
                                        allListRefresh(res);
                                    }
                                ).catch(
                                    (err) => {
                                        console.log('Got error: ' + err);
                                        openErrorMsg(err);
                                    }
                                );
                                console.log("sup arc : " + edge.source.value + "->" + edge.target.value);
                            }
                            console.log("add arc :" + edge.source.value + "->" + terminal.value);
                            addArc(convertCellToId(edge.source), convertCellToId(terminal)).then(
                                (res) => {
                                    return refreshGraph();
                                }
                            ).then(
                                (res) => {
                                    allListRefresh(res);
                                }
                            ).catch(
                                (err) => {
                                    console.log('Got error: ' + err);
                                    openErrorMsg(err);
                                }
                            );
                        } else {
                            console.log("? ->" + terminal.value);
                        }
                    }
                }
            }

            // console.log(edge);
            // console.log(terminal); 
            // console.log(source);
            // console.log(constraint );

            graphCellConnected.apply(this, arguments);
        }

        // Allows to save current and last selection
        graph.getSelectionModel().addListener(mxEvent.CHANGE, function(sender, evt) {

            /// not obvious but keep the current selection
            var cells = evt.getProperty('removed');
            if (typeof(cells) === 'undefined') {
                cells = null;
            }
            lastSelection = selection;
            selection = cells;

        });

        // Ctrl + click on nothing : add node
        // Ctrl + click on something : remove the something
        // Shift + click on node then Shift + click on a second node : add edge between node 
        graph.addListener(mxEvent.CLICK, function(sender, evt) {
            var e = evt.getProperty('event'); // mouse event
            var cell = evt.getProperty('cell'); // cell may be null

            if (e.ctrlKey || virtualCtrlBtnActivate) {
                if (cell != null) {
                    //console.log(cell);
                    if (cell.vertex) {
                        removeNode(convertCellToId(cell)).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );
                    } else if (cell.edge) {
                        removeArc(convertCellToId(cell.source), convertCellToId(cell.target)).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );
                    }
                } else {
                    var pt = graph.getPointForEvent(e);
                    let name = "N" + nodeCounter;
                    nodeCounter++;
                    let nbrmod = 2;
                    if (pt.x && pt.y) {
                        addNode(name, nbrmod, pt.x, pt.y).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );
                    } else {
                        addNode(name, nbrmod, 0, 0).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );
                    }
                }
            } else if ((e.shiftKey || virtualShiftBtnActivate) && cell && cell.vertex) {
                if (arcSource) {
                    if (arcSource.value !== cell.value) {
                        addArc(convertCellToId(arcSource), convertCellToId(cell)).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );
                    }
                    arcSource = cell;
                } else if (lastSelection) {
                    for (let index = 0; index < lastSelection.length; index++) {
                        if (lastSelection[index] && lastSelection[index].vertex && (lastSelection[index].value !== cell.value))
                            addArc(convertCellToId(lastSelection[index]), convertCellToId(cell)).catch(
                                (err) => {
                                    console.log('Got error: ' + err);
                                    openErrorMsg(err);
                                }
                            );
                    }
                    refreshGraph().then(
                        (res) => {
                            allListRefresh(res);
                        }
                    ).catch(
                        (err) => {
                            console.log('Got error: ' + err);
                            openErrorMsg(err);
                        }
                    );
                    arcSource = cell;
                }
            } else {
                if (cell && cell.vertex) {
                    nodeSelectionId = convertCellToId(cell);
                    refreshNodeMenu();
                    arcSource = cell;
                } else {
                    nodeSelectionId = null;
                    foldAllMenus(0);
                    arcSource = null;
                }

            }

            closeErrorMsg();
        });

        graph.addListener(mxEvent.DOUBLE_CLICK, function(sender, evt) {
            var e = evt.getProperty('event'); // mouse event
            var cell = evt.getProperty('cell'); // cell may be null

            // allow to open the node menu 
            if (cell && cell.vertex) {
                // make it appear
                refreshNodeMenu();
                open_menu("node_menu");
            }
        });

        // event to manage "del"
        if (keyHandler == null) {
            keyHandler = new mxKeyHandler(graph);
        }
        keyHandler.bindKey(46, function(evt) {

            let cells = graph.getSelectionModel().cells;
            // if only one selected, it is removed edge or node 
            if (cells.length == 1) {
                cells.forEach(el => {
                    if (el && el.vertex) {
                        removeNode(convertCellToId(el)).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );
                    } else if (el && el.edge) {
                        let idS = convertCellToId(el.source);
                        let idT = convertCellToId(el.target);
                        removeArc(idS, idT).then(
                            (res) => {
                                return refreshGraph();
                            }
                        ).then(
                            (res) => {
                                allListRefresh(res);
                            }
                        ).catch(
                            (err) => {
                                console.log('Got error: ' + err);
                                openErrorMsg(err);
                            }
                        );

                    }
                });
                // if multiple element, only the nodes are removed
            } else if (cells.length > 1) {
                let idList = [];
                cells.forEach(el => {
                    if (el && el.vertex) {
                        idList.push(convertCellToId(el));
                    }
                });
                idList.forEach(el => {
                    removeNode(el);
                });

                refreshGraph().then(
                    (res) => {
                        allListRefresh(res);
                    }
                ).catch(
                    (err) => {
                        console.log('Got error: ' + err);
                        openErrorMsg(err);
                    }
                );

            }
        });

        let keyList = [
            [48, 0],
            [96, 0],
            [49, 1],
            [97, 1],
            [50, 2],
            [98, 2],
            [51, 3],
            [99, 3],
            [52, 4],
            [100, 4]
        ];

        keyList.forEach(el => {
            keyHandler.bindKey(el[0], function(evt) {
                changeGroupCellSelection(el[1]);
            });
        });

        let arrowKeyList = [
            [37, "left"],
            [38, "up"],
            [39, "right"],
            [40, "bottom"]
        ];

        arrowKeyList.forEach(el => {
            keyHandler.bindKey(el[0], function(evt) {
                moveGroupCellSelection(el[1]);
            });
        });

        // click "h" to open help menu
        keyHandler.bindKey(72, function(evt) {
            let helpdiv = document.getElementById("helpDiv");
            helpdiv.style.display = 'block';
        });

        // click "d" to open help menu
        keyHandler.bindKey(68, function(evt) {
            toggle_menu("debug_menu", 1);
        });

        // Ctrl + a : select ALL
        keyHandler.bindControlKey(65, function(evt) {
            graph.selectAll();
        });

        mxEvent.addMouseWheelListener(function(evt, up) {
            Print = false;
            if (evt.ctrlKey && up) {

                graph.zoomIn();
                mxEvent.consume(evt);
            } else if (evt.ctrlKey) {
                graph.zoomOut();
                mxEvent.consume(evt);
            }
        });

    }
}

/**
 *  Defines all option and creates the canvas for the inference graph
 *  @param {object} container Container Object around the graph.
 */
var graphToInfMxgraph = function(container) {

    container.innerHTML = "";

    // Checks if the browser is supported
    if (!mxClient.isBrowserSupported()) {
        // Displays an error message if the browser is not supported.
        mxUtils.error('Browser is not supported!', 200, false);
    } else {
        // Fixes possible clipping issues in Chrome
        mxClient.NO_FO = true;

        // Disables the built-in context menu
        mxEvent.disableContextMenu(container);

        // Changes some default colors
        mxConstants.HANDLE_FILLCOLOR = 'none';
        mxConstants.HANDLE_STROKECOLOR = 'none';
        mxConstants.VERTEX_SELECTION_COLOR = '#FFB83C';
        mxConstants.EDGE_SELECTION_COLOR = 'none';

        // Creates the graph inside the given container
        graph = new mxGraph(container);

        // Set Zoom factor
        graph.zoomFactor = 1.05;

        // Enables rubberband selection
        new mxRubberband(graph);

        // Enables the moving canvas with dragging with right click 
        graph.setPanning(true);

        // Adds custom HTML labels
        graph.setHtmlLabels(true);

        graph.setAllowDanglingEdges(false);
        graph.setDisconnectOnMove(false);
        graph.setCellsEditable(false);

        // Override folding to allow for tables
        graph.isCellFoldable = function(cell, collapse) {
            return this.getModel().isVertex(cell);
        };

        redrawInfGraph(0);

        //graph.foldCells(true);

        var graphCellsMoved = graph.cellsMoved;
        graph.cellsMoved = function(cells, dx, dy, disconnect, constrain, extend) {

            cells.forEach(cell => {
                if (cell.vertex) {
                    var nodeId = convertCellToId(cell);
                    var geo = cell.getGeometry();
                    if (nodeId != undefined) {
                        moveNodeInf(nodeId, geo.x + dx, geo.y + dy)
                    }
                }
            });

            graphCellsMoved.apply(this, arguments);
        };

        var vertexHandlerUnion = mxVertexHandler.prototype.union;
        mxVertexHandler.prototype.union = function(bounds, dx, dy, index, gridEnabled, scale, tr) {
            var result = vertexHandlerUnion.apply(this, arguments);

            result.width = Math.max(result.width, mxUtils.getNumber(this.state.style, 'minWidth', 0));
            result.height = Math.max(result.height, mxUtils.getNumber(this.state.style, 'minHeight', 0));

            return result;
        };


        graph.addListener(mxEvent.CLICK, function(sender, evt) {
            var e = evt.getProperty('event'); // mouse event
            var cell = evt.getProperty('cell'); // cell may be null

            if (e.ctrlKey) {

            } else if (e.shiftKey && cell && cell.vertex) {

            } else {
                // allow to open the node menu 
                if (cell && cell.vertex) {
                    // make it appear
                    nodeSelectionId = convertCellToId(cell);
                    refreshInfNodeMenu();
                } else {
                    nodeSelectionId = null;
                    foldAllMenus(0);
                }

            }

            closeErrorMsg();
        });

        // Allows to save current and last selection
        graph.getSelectionModel().addListener(mxEvent.CHANGE, function(sender, evt) {

            /// not obvious but keep the current selection
            var cells = evt.getProperty('removed');
            if (typeof(cells) === 'undefined') {
                cells = null;
            }
            lastSelection = selection;
            selection = cells;

        });

        graph.addListener(mxEvent.DOUBLE_CLICK, function(sender, evt) {
            var e = evt.getProperty('event'); // mouse event
            var cell = evt.getProperty('cell'); // cell may be null

            // allow to open the node menu 
            if (cell && cell.vertex) {
                // make it appear
                refreshInfNodeMenu();
                open_menu("inf_node_menu");
            }
        });

        // Remove event to manage "del"
        keyHandler.bindKey(46, function(evt) {
            // Do Nothing
        });

        // Ctrl + a : select ALL
        keyHandler.bindControlKey(65, function(evt) {
            graph.selectAll();
        });

        let keyList = [
            [48, 0],
            [96, 0],
            [49, 1],
            [97, 1],
            [50, 2],
            [98, 2],
            [51, 3],
            [99, 3],
            [52, 4],
            [100, 4]
        ];

        keyList.forEach(el => {
            keyHandler.bindKey(el[0], function(evt) {
                changeGroupCellSelection(el[1]);
            });
        });

        let arrowKeyList = [
            [37, "left"],
            [38, "up"],
            [39, "right"],
            [40, "bottom"]
        ];

        arrowKeyList.forEach(el => {
            keyHandler.bindKey(el[0], function(evt) {
                moveGroupCellSelection(el[1]);
            });
        });

        mxEvent.addMouseWheelListener(function(evt, up) {
            Print = false;
            if (evt.ctrlKey && up) {
                graph.zoomIn();
                mxEvent.consume(evt);
            } else if (evt.ctrlKey) {
                graph.zoomOut();
                mxEvent.consume(evt);
            }
        });

    }
};