/** @module Tables utilities */

/**
 * Function called when the value changes in the Cpt input
 * @param {object} that Element to modify.
 */
var changeBackgroundColor = function(that) {
    that.style.backgroundColor = 'rgb(' + (255 - that.value * 128) + ',' + (127 + that.value * 128) + ', 100)';
}

/**
 * Creates the table of Cpt
 * @param {json} jsonCpt Json with the Cpt values. 
 * @param {string} divId Id of the div parent.
 */
var computeCptIntoTable = function(jsonCpt, divId) {

    let div = document.getElementById(divId);
    div.innerHTML = '';

    // creates a <table> element and a <tbody> element
    var tbl = document.createElement("table");
    tbl.className = "cptTable";
    var tblBody = document.createElement("tbody");

    // creates first line
    var nbparents = 0
    if (jsonCpt["parents"]) {
        nbparents = jsonCpt["parents"].length;
    }
    var row = document.createElement("tr");
    var cell;
    if (nbparents > 0) {
        cell = document.createElement("th");
        cell.colSpan = nbparents;
        row.appendChild(cell);
    }
    cell = document.createElement("th");
    cell.style.backgroundColor = '#AAAAAA';
    cell.colSpan = jsonCpt["node"].size;
    cell.textContent = (jsonCpt["node"].name);
    row.appendChild(cell);
    tblBody.appendChild(row);

    // creates second line
    row = document.createElement("tr");
    if (nbparents > 0) {
        for (var i = 0; i < nbparents; i++) {
            cell = document.createElement("th");
            cell.style.backgroundColor = '#AAAAAA';
            cell.innerText = jsonCpt["parents"][i].name;
            cell.setAttribute('title', jsonCpt["parents"][i].name);
            row.appendChild(cell);
        }
    }
    for (var i = 0; i < jsonCpt["node"].size; i++) {
        cell = document.createElement("th");
        cell.style.backgroundColor = '#CCCCCC';
        cell.innerText = jsonCpt["node"]["labels"][i];
        cell.setAttribute('title', jsonCpt["node"]["labels"][i]);
        row.appendChild(cell);
    }
    tblBody.appendChild(row);

    // creates the following lines
    let offset = [];
    let index = [];
    let off = 1;
    for (let j = 0; j < nbparents; j++) {
        offset[j] = off;
        off *= jsonCpt["parents"][j].labels.length;
        index[j] = 0;
    }
    let line = 0;
    let nbLines = jsonCpt["data"].length / jsonCpt["node"].size;
    let dataIdx = 0;
    while (line != nbLines) {
        row = document.createElement("tr");
        for (var par = 0; par < nbparents; par++) {
            let sizeParent = jsonCpt["parents"][par].labels.length;
            if ((line % offset[par]) == 0) {
                cell = document.createElement("th");
                cell.style.backgroundColor = '#CCCCCC';
                cell.rowSpan = offset[par];
                cell.innerText = jsonCpt["parents"][par].labels[index[par] % sizeParent];
                cell.setAttribute('title', jsonCpt["parents"][par].labels[index[par] % sizeParent]);
                index[par]++;
                row.appendChild(cell);
            }

        }

        for (var i = 0; i < jsonCpt["node"].size; i++) {
            let value = jsonCpt["data"][dataIdx].toFixed(4);
            cell = document.createElement("td");
            let input = document.createElement("input");
            input.type = "text";
            input.style.textAlign = 'right';
            input.value = value;
            input.style.backgroundColor = 'rgb(' + (255 - value * 128) + ',' + (127 + value * 128) + ', 100)';
            input.className = "tab_input";
            input.id = "inputCpt" + dataIdx;
            input.addEventListener('change', function(evt) {
                changeBackgroundColor(this);
            });

            cell.appendChild(input);
            row.appendChild(cell);
            dataIdx++;
        }
        tblBody.appendChild(row);
        line++;
    }

    // put the <tbody> in the <table>
    tbl.appendChild(tblBody);
    // appends <table> into <body>
    div.appendChild(tbl);
}