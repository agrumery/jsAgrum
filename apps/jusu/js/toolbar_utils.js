/** @module Toolbar utilities */

// Global variables
/** keep the graph mode (0: graphical, 1: inference) */
var graphMode = 0;

/** Mobile Ctrl and Shift virtualization for mobile */
var virtualCtrlBtnActivate = 0;
var virtualShiftBtnActivate = 0;

/**
 * Closes all the menus
 * @param {boolean} force Close also debug menu. 
 */
var foldAllMenus = function(force) {
    let subMenu = document.getElementById("submenu");
    let childsSubmenu = subMenu.children;
    for (var i = 0; i < childsSubmenu.length; i++) {
        if (childsSubmenu[i].id != "debug_menu" || force) {
            childsSubmenu[i].style.display = 'none';
        }
    }

    subMenu.style.display = 'none';

    let allContainer = document.getElementById("columns");
    allContainer.style.gridTemplateColumns = '';
}

/**
 * Opens a lateral menu
 * @param {string} id_menu div Id of the menu.
 */
var open_menu = function(id_menu) {
    let allContainer = document.getElementById("columns");
    let menuItem = document.getElementById(id_menu);
    let btnMenu = document.getElementById("btnMenu");
    let subMenu = document.getElementById("submenu");

    allContainer.style.gridTemplateColumns = '3fr 1fr';
    menuItem.style.display = 'block';
    btnMenu.style.display = 'flex';
    subMenu.style.display = 'block';
}

/**
 * Creates the toggle mode for the toolbar menu
 * @param {string} id_menu div Id of the menu.
 * @param {boolean} force  Close also debug menu.
 */
var toggle_menu = function(id_menu, force) {
    let allContainer = document.getElementById("columns");
    let menuItem = document.getElementById(id_menu);
    let btnMenu = document.getElementById("btnMenu");
    let subMenu = document.getElementById("submenu");
    if (menuItem.style.display == 'none') {
        allContainer.style.gridTemplateColumns = '3fr 1fr';
        menuItem.style.display = 'block';
        btnMenu.style.display = 'flex';
        subMenu.style.display = 'block';
    } else {
        foldAllMenus(force);
    }
}

/**
 * Creates the toggle mode for the virtual Ctrl btn
 */
var toggleVirtualCtrlBtn = function() {

    var btn = document.getElementById("mobileTouchNode");

    // des-active the Shift toggle btn
    if (virtualShiftBtnActivate) {
        toggleVirtualShiftBtn();
    }

    if (virtualCtrlBtnActivate) {
        virtualCtrlBtnActivate = 0;
        btn.src = 'img/node.svg';
    } else {
        virtualCtrlBtnActivate = 1;
        btn.src = 'img/nodeb.svg';
    }
}

/**
 * Creates the toggle mode for the virtual Shift btn
 */
var toggleVirtualShiftBtn = function() {

    var btn = document.getElementById("mobileTouchEdge");

    // des-active the Ctrl toggle btn
    if (virtualCtrlBtnActivate) {
        toggleVirtualCtrlBtn();
    }

    if (virtualShiftBtnActivate) {
        virtualShiftBtnActivate = 0;
        btn.src = 'img/edge.svg';
    } else {
        virtualShiftBtnActivate = 1;
        btn.src = 'img/edgeb.svg';
    }
}

/** 
 * Modify the addItem to remove the double click / touchend 
 */
var mwToolbarAddItem = mxToolbar.prototype.addItem;
mxToolbar.prototype.addItem = function(title, icon, funct, pressedIcon, style, factoryMethod) {

    let img = mwToolbarAddItem.apply(this, arguments);

    // Invokes the function on a click on the toolbar item
    if (funct != null) {

        if (mxClient.IS_TOUCH) {
            mxEvent.removeListener(img, 'click', funct);
        }

    }

    return img;
};

var printBtnToolbar = function() {
    var pageCount = 1;
    // detection if Electron
    if (!(window && window.process && window.process.type)) {
        pageCount = mxUtils.prompt('Enter page count', '1');
        if (pageCount != null) {
            printingState = 1;
            var scale = mxUtils.getScaleForPageCount(pageCount, graph);
            var preview = new mxPrintPreview(graph, scale);
            preview.open();
            printingState = 0;
        }
    } else {
        console.log("not available yet!");
        document.getElementById("errorDiv").style.display = "flex";
        document.getElementById("errorMsg").textContent = "not available yet!";
    }
}

/**
 * Creates the toolbar on the top
 */
var createToolBar = function() {

    // Remove the last menu
    var container = document.getElementById("menu");
    container.innerHTML = '';

    // Create structure
    var toolbar = new mxToolbar(container);

    var elt = toolbar.addSeparator('img/separator.svg');

    // standard block
    if (graphMode == 0) {
        elt = toolbar.addItem("New BN Graph", 'img/file.svg', function() {
            toggle_menu("new_page_menu", 0);
        }, null, null, null);


        elt = toolbar.addItem("Upload BN Graph", 'img/upload.svg', function() {
            toggle_menu("upload_menu", 0);
        }, null, null, null);


        elt = toolbar.addItem("Save BN Graph", 'img/download.svg', function() {
            //toggle_menu("download_menu", 0);
            saveBN();
        }, null, null, null);

        elt = toolbar.addItem('Print', 'img/printer.svg', function(evt) {
            printBtnToolbar();
        });
        elt.setAttribute('id', "printPoster");

    } else {
        elt = toolbar.addItem("New BN Graph", 'img/fileg.svg', function() {}, null, null, null);


        elt = toolbar.addItem("Upload BN Graph", 'img/uploadg.svg', function() {}, null, null, null);


        elt = toolbar.addItem("Save BN Graph", 'img/download.svg', function() {
            saveBN();
        }, null, null, null);

        elt = toolbar.addItem('Print', 'img/printer.svg', function(evt) {
            printBtnToolbar();
        });
        elt.setAttribute('id', "printPoster");
    }

    elt = toolbar.addSeparator('img/separator.svg');

    // zoom layout block
    elt = toolbar.addItem("Zoom Fit", 'img/zoomz.svg', function() {
        //graph.fit();
        var margin = 100;
        var max = 3;

        var bounds = graph.getGraphBounds();
        var cw = graph.container.clientWidth - margin;
        var ch = graph.container.clientHeight - margin;
        var w = bounds.width / graph.view.scale;
        var h = bounds.height / graph.view.scale;
        var s = Math.min(max, Math.min(cw / w, ch / h));

        graph.view.scaleAndTranslate(s,
            (margin + cw - w * s) / (2 * s) - bounds.x / graph.view.scale,
            (margin + ch - h * s) / (2 * s) - bounds.y / graph.view.scale);

    }, null, null, null);

    elt = toolbar.addItem("Zoom -", 'img/zoomm.svg', function() {
        graph.zoomOut();
    }, null, null, null);

    elt = toolbar.addItem("Zoom +", 'img/zoomp.svg', function() {
        graph.zoomIn();
    }, null, null, null);

    elt = toolbar.addItem("Layout", 'img/layout.svg', function() {
        var elt = document.getElementById("layoutCombo");
        if (elt.selectedIndex !== -1) {
            applyNewLayout(elt.options[elt.selectedIndex].value);
        }
    }, null, null, null);

    var combo = toolbar.addCombo(null);
    combo.id = "layoutCombo";

    elt = toolbar.addOption(combo, 'Hierarchical', "Hierarchical");
    elt = toolbar.addOption(combo, 'Circle', "Circle");
    elt = toolbar.addOption(combo, 'FastOrganic', "FastOrganic");
    elt.selected = "selected";

    combo.addEventListener("change", function() {
        if (this.selectedIndex !== -1) {
            applyNewLayout(this.options[this.selectedIndex].value);
        }
    });

    elt = toolbar.addSeparator('img/separator.svg');

    // color block
    elt = toolbar.addItem("Group 0", 'img/g0.svg', function() {
        changeGroupCellSelection(0);
    }, null, null, null);

    elt = toolbar.addItem("Group 1", 'img/g1.svg', function() {
        changeGroupCellSelection(1);
    }, null, null, null);

    elt = toolbar.addItem("Group 2", 'img/g2.svg', function() {
        changeGroupCellSelection(2);
    }, null, null, null);

    elt = toolbar.addItem("Group 3", 'img/g3.svg', function() {
        changeGroupCellSelection(3);
    }, null, null, null);

    elt = toolbar.addItem("Group 4", 'img/g4.svg', function() {
        changeGroupCellSelection(4);
    }, null, null, null);

    elt = toolbar.addSeparator('img/separator.svg');

    // optional block
    if (graphMode == 0) {

        elt = toolbar.addItem("Load and Learn CSV file", 'img/csv_format.svg', function() {
            toggle_menu("learning_menu", 0);
        }, null, null, null);

        elt = toolbar.addItem("Generate Random CPT", 'img/random.svg', function() {
            genCPTs();
        }, null, null, null);

        elt = toolbar.addSeparator('img/separator.svg');
        //elt = toolbar.addSeparator('img/double_space.svg');

        // keep the button for all device
        //if (mobileDevice) 
        {
            elt = toolbar.addItem("Mobile Ctrl activation", 'img/node.svg', function() {
                toggleVirtualCtrlBtn();
            }, null, null, null);
            elt.setAttribute('id', "mobileTouchNode");

            elt = toolbar.addItem("Mobile Shift activation", 'img/edge.svg', function() {
                toggleVirtualShiftBtn();
            }, null, null, null);
            elt.setAttribute('id', "mobileTouchEdge");

        }

    } else {
        elt = toolbar.addItem("fold all node", 'img/foldNb.svg', function() {
            let listNode = graph.getChildVertices(graph.getDefaultParent());
            graph.foldCells(false, true, listNode);
        }, null, null, null);

        elt = toolbar.addItem("expand all node", 'img/expandNb.svg', function() {
            let listNode = graph.getChildVertices(graph.getDefaultParent());
            graph.foldCells(true, true, listNode);
        }, null, null, null);
    }

    elt = toolbar.addSeparator('img/separator.svg');


    /*elt = toolbar.addItem("Debug Menu", 'img/debug.svg', function() {
        toggle_menu("debug_menu", 1);
    }, null, null, null);*/

}

/**
 * Moves to Inference mode
 */
var goInfMode = function() {
    console.log("Go into the Inference mode");
    graphMode = 1;
    resetEvidenceEngine();
    let view = graph.getView();
    let vScale = view.getScale();
    let gridContainer = document.getElementById('graphContainer');
    graphToInfMxgraph(gridContainer);
    refreshGraph().then(
        (res) => {
            allListRefresh(res);
        }
    ).catch(
        (err) => {
            console.log('Got error: ' + err);
            openErrorMsg(err);
        }
    );
    graph.zoomTo(vScale, false);
    foldAllMenus();
    createToolBar();
    document.getElementById("jusuFiligram").style.backgroundColor = '#effaf4';
    document.getElementById("graph_nav").classList.remove('tab_active');
    document.getElementById("inf_nav").classList.add('tab_active');

}

/** 
 * Moves to graphical mode
 */
var goGraphMode = function() {
    console.log("Go into the Graphic mode");
    graphMode = 0;
    mxHardEvidence = null;
    mxPosteriorList = {};
    let view = graph.getView();
    let vScale = view.getScale();
    let gridContainer = document.getElementById('graphContainer');
    graphToMxgraph(gridContainer);
    refreshGraph().then(
        (res) => {
            allListRefresh(res);
        }
    ).catch(
        (err) => {
            console.log('Got error: ' + err);
            openErrorMsg(err);
        }
    );
    graph.zoomTo(vScale, false);
    foldAllMenus();
    createToolBar();
    document.getElementById("jusuFiligram").style.backgroundColor = 'white';
    document.getElementById("graph_nav").classList.add('tab_active');
    document.getElementById("inf_nav").classList.remove('tab_active');
}