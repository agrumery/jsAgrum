/** @module Events utilities */

// Global variables
/** Number for the name creation */
var nodeCounter = 0;

/** Object use to read Bif file */
var readerBif = new FileReader();
/** Object use to read CSV file */
var readerCSV = new FileReader();


/** variable to know if touchscreen */
var mobileDevice;

/**
 * @method
 * Checks if it is a mobile /tablet 
 */
window.mobilecheck = function() {
    var check = false;
    (function(a) { if (/(Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera, 'http://detectmobilebrowser.com/mobile');
    return check;
};

/**
 * Closes the Error message
 */
var closeErrorMsg = function() {
    document.getElementById("errorDiv").style.display = 'none';
};

/**
 * Closes the Help message
 */
var closeHelpMsg = function() {
    document.getElementById("helpDiv").style.display = 'none';
};

/**
 * Opens the Error message
 * @param {string} mes Error Message. 
 */
var openErrorMsg = function(mes) {
    if (mes.split(']')[1]) {
        document.getElementById("errorMsg").textContent = mes.split(']')[1];
    } else {
        document.getElementById("errorMsg").textContent = mes;
    }
    document.getElementById("errorDiv").style.display = "flex";
}

/**
 * @method
 * Creates all the "click" and other listener
 */
window.onload = function() {

    // detect mobile Device function
    mobileDevice = window.mobilecheck();

    // Btn Menu
    document.getElementById("expendBtn").addEventListener("click", function() {
        let allContainer = document.getElementById("columns");
        let submenu = document.getElementById("submenu");
        let icon = document.getElementById("expendBtn");
        if (allContainer.style.gridTemplateColumns == '3fr 1fr') {
            allContainer.style.gridTemplateColumns = '1fr 3fr';
            submenu.style.width = "80vw";
            icon.style.backgroundImage = "url(img/right_arrow.svg)";
        } else {
            allContainer.style.gridTemplateColumns = '3fr 1fr';
            submenu.style.width = "30vw";
            icon.style.backgroundImage = "url(img/left_arrow.svg)";
        }

    });
    document.getElementById("closeMenuBtn").addEventListener("click", function() {
        foldAllMenus(1);
    });

    // Creation Menu
    document.getElementById("createBtn").addEventListener("click", function() {
        let name = document.getElementById("bnName").value;
        createBN(name).then(
            (res) => {
                foldAllMenus(0);
                return refreshGraph();
            }
        ).then(
            (res) => {
                allListRefresh(res);
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

    // Node menu Event
    document.getElementById("applyNodeBtn").addEventListener("click", function() {
        let nbMode = document.getElementById("nbrm").value;
        let tname = document.getElementById("nodeName").value;
        let oldNbrMod = Object.keys(mxNodesList[nodeSelectionId].labels).length;
        if (nbMode != oldNbrMod) {

            if (tname == "") {
                tname = mxNodesList[nodeSelectionId].name;
            }
            changeNodeModeNumber(nodeSelectionId, tname, parseInt(nbMode));
        } else {
            if (tname != "") {
                changeNodeName(nodeSelectionId, tname).then(
                    (res) => {
                        return refreshGraph();
                    }
                ).then(
                    (res) => {
                        allListRefresh(res);
                    }
                ).catch(
                    (err) => {
                        console.log('Got error: ' + err);
                        openErrorMsg(err);
                    }
                );
            }
        }
    });

    document.getElementById("changeLabelNameBtn").addEventListener("click", function() {
        //let tableName = document.getElementById("nodeLabelList").value;
        let count = 0;
        let json_names = {};
        let labelsNb = Object.keys(mxNodesList[nodeSelectionId].labels).length;
        for (i = 0; i < labelsNb; i++) {
            let name_input = "label-" + i;
            let new_name = document.getElementById(name_input).value;
            if (!isNaN(new_name)) {
                new_name = new_name.toString();
            }
            let old_name = mxNodesList[nodeSelectionId].labels[i];
            if (!isNaN(old_name)) {
                old_name = old_name.toString();
            }
            if (new_name != "") {
                json_names[old_name] = new_name;
                count++;
            }
        }
        if (count) {
            let json_txt = JSON.stringify(json_names);
            console.log(json_txt);
            changeLabelsName(nodeSelectionId, json_txt).then(
                (res) => {
                    return refreshGraph();
                }
            ).then(
                (res) => {
                    allListRefresh(res);
                }
            ).catch(
                (err) => {
                    console.log('Got error: ' + err);
                    openErrorMsg(err);
                }
            );
        }

    });

    document.getElementById("applyCptBtn").addEventListener("click", function() {
        let list_input = document.getElementsByClassName("tab_input");

        // creates value array
        let values = [];
        for (let i = 0; i < list_input.length; i++) {
            values.push(parseFloat(list_input[i].value));
        }
        // creates Json Data
        let sendData = {};
        sendData["pValues"] = values;

        addCpt(nodeSelectionId, JSON.stringify(sendData)).then(
            (res) => {
                return refreshGraph();
            }
        ).then(
            (res) => {
                allListRefresh(res);
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

    document.getElementById("applyGroupNodeBtn").addEventListener("click", function() {
        let numGroup = document.getElementById("numg").value;
        let group = parseFloat(numGroup);
        setGroupNode(nodeSelectionId, group);
        let cell = recoverCell(mxNodesList[nodeSelectionId].name);
        changeStyleGroupCell(cell, group);
        mxNodesList[nodeSelectionId]["group"] = group;
    });

    // Download Menu
    document.getElementById("saveBnBtn").addEventListener("click", function() {
        let name = document.getElementById("saveName").value;
        saveBN().then(
            (data) => {
                var zip = new JSZip();
                zip.file((data.filename + ".o3prm"), data.contents);
                zip.file((data.filename + ".jusu"), data.posfile);
                zip.generateAsync({ type: "blob" })
                    .then(function(blob) {
                        download(blob, (data.filename + ".vbn"), "text/plain");
                    });
            }
        );
    });

    // Upload Menu
    document.getElementById("loadBnBtn").addEventListener("change", function() {
        console.log(this.files[0]);
        readerBif.fileName = this.files[0].name
        readerBif.readAsText(this.files[0], 'UTF-8');
    });

    readerBif.addEventListener('load', function() {
        loadBN(readerBif.fileName, readerBif.result, "{}", 1).then(
            (res) => {
                foldAllMenus(0);
                document.getElementById("loading_icon").style.display = 'none';
                return refreshGraph();
            }
        ).then(
            (res) => {
                allListRefresh(res);
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

    document.getElementById("loadVBnBtn").addEventListener("change", function() {
        console.log(this.files[0]);
        loadVBNFromData(this.files[0]);
    });

    document.getElementById("fileFetchBtn").addEventListener("click", function() {
        let addr = document.getElementById('filefetchInput').value;
        loadVBNFromAdrr(addr);
    });

    // Learning Menu
    document.getElementById("loadCSVBtn").addEventListener("change", function() {
        console.log(this.files[0]);
        readerCSV.readAsText(this.files[0], 'UTF-8');
    });

    readerCSV.addEventListener('load', function() {
        let name = document.getElementById("bnNameLoad").value;
        loadCSV(readerCSV.result, name).then(
            (res) => {
                foldAllMenus(0);
                return refreshGraph();
            }
        ).then(
            (res) => {
                allListRefresh(res);
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

    // Error message
    document.getElementById("errorDiv").addEventListener("click", function(event) {
        closeErrorMsg();
    });

    // Help message
    document.getElementById("closeHelp").addEventListener("click", function(event) {
        closeHelpMsg();
    });

    // UI Events (debug)
    document.getElementById("refreshBtn").addEventListener("click", function() {
        toStringBN().then(
            (data) => {
                document.getElementById("graphText").textContent = data.string;
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

    document.getElementById("refreshDotBtn").addEventListener("click", function() {
        toDot().then(
            (data) => {
                document.getElementById("graphTextDot").textContent = data.string;
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

    document.getElementById("sendCptBtn").addEventListener("click", function() {
        let jsonId = document.getElementById("jsonId").value;
        let jsontxt = document.getElementById("json_txt").value;
        addCpt(parseInt(jsonId), jsontxt).then(
            (res) => {
                return refreshGraph();
            }
        ).then(
            (res) => {
                allListRefresh(res);
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

    document.getElementById("resetIEBtn").addEventListener("click", function() {
        resetEvidenceEngine().catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    });

};