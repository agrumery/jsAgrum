/** @module Chart utilities */


/**
 * Checks if the hard evidence is already apply (by label Id)
 * @param {integer} node Node Id. 
 * @param {integer} labelIdx Label Id.
 */
function isHardEvidenceAlreadyDone(node, labelIdx) {

    if (mxHardEvidence) {
        let size = Object.keys(mxHardEvidence).length
        for (let i = 0; i < size; i++) {
            if (mxHardEvidence[i] && mxHardEvidence[i].id == node && mxHardEvidence[i].value == labelIdx) {
                return true;
            }
        }
    }

    return false;
}

/**
 * handles the toggle mode when a bar graph is double clicked
 * @param {integer} node Node Id. 
 * @param {integer} labelIdx Label Id.
 */
function chartToggleEvidence(node, labelIdx) {

    if (!isHardEvidenceAlreadyDone(node, labelIdx)) {
        // send msg to add the hard evidence
        addEvidence(node, labelIdx).then(
            (res) => {
                return hardEvidence();
            }
        ).then(
            (data) => {
                hardEvidenceListRefresh(data);
                refreshChartGraph();
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    } else {
        eraseEvidence(node).then(
            (res) => {
                return hardEvidence();
            }
        ).then(
            (data) => {
                hardEvidenceListRefresh(data);
                refreshChartGraph();
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    }
}

/**
 * find the index of a Label of the selected node
 * @param {string} name Label Name
 */
function findIndexOfLabelName(name) {
    if (mxNodesList) {
        let array = mxNodesList[nodeSelectionId].labels;
        for (let index = 0; index < array.length; index++) {
            if (array[index] == name) {
                return index;
            }

        }
    }
    return -1;
}

/**
 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
 * 
 * @param {String} text The text to be rendered.
 * @param {String} font The css font descriptor that text is to be rendered with (e.g. "bold 14px verdana").
 * 
 * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
 */
function getTextWidth(text, font) {
    // re-use canvas object for better performance
    var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return metrics.width;
}

function updateLabelChart(name) {
    let divId = "#" + computeIdChart(name);
    d3.select(divId + " svg").remove();
    let widthB = chartArray[name].getAttribute("width");
    let heightB = chartArray[name].getAttribute("height");
    drawChart(divId, mxPosteriorList[name], name, widthB, heightB);
}

function drawChart(id, data, name, widthB, heightB) {

    //find the max label name size to adapt the left margin
    let max_width = 0;
    data.forEach(element => {
        let result = getTextWidth(element.name, "11px arial")
        if (result > max_width) {
            max_width = result;
        }
    });

    //adds some margin to the text size
    max_width += 15;

    //set up svg using margin conventions - we'll need plenty of room on the left for labels
    var margin = {
        top: 30,
        right: 25,
        bottom: 15,
        left: max_width
    };

    var width = widthB - margin.left - margin.right,
        height = heightB - margin.top - margin.bottom;

    var colorFill = ["rgba(255, 99, 132, 0.2)",
        "rgba(255, 159, 64, 0.2)",
        "rgba(255, 205, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(201, 203, 207, 0.2)"
    ];
    var colorStroke = ["rgb(255, 99, 132)",
        "rgb(255, 159, 64)",
        "rgb(255, 205, 86)",
        "rgb(75, 192, 192)",
        "rgb(54, 162, 235)",
        "rgb(153, 102, 255)",
        "rgb(201, 203, 207)"
    ];

    var svg = d3.select(id).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 3))
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .text(name);

    // create a list with 0 to extent the axis
    let dataExtent = [...data];
    dataExtent.push({ value: 0 });

    var x = d3.scaleLinear()
        .range([0, width])
        .domain(d3.extent(dataExtent, function(d) {
            let value = d.value + (10 * d.value) / 100;
            if (value > 1) {
                value = 1;
            }
            return value;
        }));

    var y = d3.scaleBand()
        .rangeRound([height, 0])
        .padding(0.1)
        .domain(data.map(function(d) {
            return d.name;
        }));

    // make x axis
    var xAxis = d3.axisBottom()
        .scale(x)
        .tickFormat(function(d) { return ""; });

    var gx = svg.append("g")
        .attr("class", "x axis")
        .call(xAxis)

    //make y axis to show bar names
    var yAxis = d3.axisLeft()
        .scale(y);

    var gy = svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)

    var bars = svg.selectAll(".bar")
        .data(data)
        .enter()
        .append("g")

    //append rects
    bars.append("rect")
        .attr("class", "bar")
        .attr("y", function(d) {
            return y(d.name);
        })
        .attr("height", y.bandwidth())
        .attr("x", 0)
        .attr("width", function(d) {
            return x(d.value);
        })
        .style("fill", function(d, i) {
            let fill = colorFill[i % 7];
            return fill;
        })
        .style("stroke", function(d, i) {
            let stroke = colorStroke[i % 7];
            return stroke;
        })
        .on("dblclick", function(d) {
            let index = findIndexOfLabelName(d.name);
            if (index != -1) {
                chartToggleEvidence(nodeSelectionId, index);
            }
        })
        .on("mouseover", function(d, i) {
            this.oldStroke = this.style.stroke;
            d3.select(this).style("stroke", "black");
        })
        .on("mouseout", function(d, i) {
            d3.select(this).style("stroke", function(d, i) {
                return this.oldStroke;
            });
        });

    //add a value label to the right of each bar
    bars.append("text")
        .attr("class", "label")
        //y position of the label is halfway down the bar
        .attr("y", function(d) {
            return y(d.name) + y.bandwidth() / 2 + 4;
        })
        .attr("x", function(d) {
            let ret = 0;
            if (d.value < 0.7) {
                ret = x(d.value) + 5;
            } else {
                ret = x(d.value) - 30;
            }
            return ret;
        })
        .text(function(d) {
            return (Math.round(d.value * 10000) / 100) + '%';
        })
        .on("dblclick", function(d) {
            console.log(d);
            let index = findIndexOfLabelName(d.name);
            if (index != -1) {
                chartToggleEvidence(nodeSelectionId, index);
            }
        });

}