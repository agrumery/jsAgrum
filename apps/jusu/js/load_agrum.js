/** @module JsAgrum utilities */

// all globals vars
/** Keeps the charts structure */
var charts = {};

/**
 * Function used when the jsAgrum worker is loaded
 */
function workerLoaded() {
    document.getElementById("loading_icon").style.display = 'none';
    document.getElementById("columns").style.display = 'grid';
    createToolBar();
    let gridContainer = document.getElementById('graphContainer');
    graphToMxgraph(gridContainer);
    createBN("BNtest").then(
        (res) => {
            foldAllMenus(0);
            return refreshGraph();
        }
    ).then(
        (res) => {
            allListRefresh(res);
        }
    ).catch(
        (err) => {
            console.log('Got error: ' + err);
            openErrorMsg(err);
        }
    );

}

/**
 * Function used when the jsAgrum worker is in Error
 * @param {object} event 
 */
function workerError(event) {
    console.log("Error: worker has met an error");
    document.getElementById("errorWorkerMsg").style.display = 'block';
    throw new Error(event.message + " (" + event.filename + ":" + event.lineno + ")");
}

/**
 * Function used when the jsAgrum UnZip a VBN
 */
function loadRefresh() {
    if (bifContent != null && jsonContent != null && typeContent != null && nameFile != null) {
        loadBN(nameFile, bifContent, jsonContent, typeContent).then(
            (res) => {
                foldAllMenus(0);
                document.getElementById("loading_icon").style.display = 'none';
                return refreshGraph();
            }
        ).then(
            (res) => {
                allListRefresh(res);
            }
        ).catch(
            (err) => {
                console.log('Got error: ' + err);
                openErrorMsg(err);
            }
        );
    }
}

// linked functions to the worker CallBacks
loadedCallBack = workerLoaded;
workerErrorCallBack = workerError;
noWorkerCallBack = () => {
    document.getElementById("graphText").innerHTML = "Sorry! No Web Worker support.";
};
loadRefreshCallBack = loadRefresh;

/**
 * Manages the refresh of the graph
 * @param {object} data Message send back by the worker
 */
function allListRefresh(data) {
    try {
        let jsonLists = JSON.parse(data.string);
        if (jsonLists) {
            mxPosList = jsonLists.NodesPosList;
            mxPosInfList = jsonLists.NodesInfPosList;
            mxArcsList = jsonLists.ArcsList;
            mxNodesList = jsonLists.NodesList;
        } else {
            mxPosList = null;
            mxPosInfList = null;
            mxArcsList = null;
            mxNodesList = null;
        }

        if (graphMode == 0) {
            redrawGraph(1);
            refreshNodeMenu();
        } else {
            redrawInfGraph(1);
        }
    } catch (error) {
        console.error(error);
    }
}

/**
 * Manages the CPT table from a worker's message
 * @param {object} data Message send back by the worker
 */
function cptRefresh(data) {
    let jsonCpt;
    try {
        jsonCpt = JSON.parse(data.string);
        computeCptIntoTable(jsonCpt, "submenu_cpt_view");
    } catch (error) {
        console.error(error);
    }
}

/**
 * Manages Hard Evidence from a worker's message
 * @param {object} data Message send back by the worker
 */
function hardEvidenceListRefresh(data) {
    try {
        let j = JSON.parse(data.string);
        if (j) {
            mxHardEvidence = j.NodesEvidenceList;
        } else {
            mxHardEvidence = null;
        }
        refreshInfNodeMenu();
        refreshColorNode();
    } catch (error) {
        console.error(error);
    }
}