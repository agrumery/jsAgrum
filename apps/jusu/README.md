# Jusu
Jusu is a web application that uses JsAgrum in a graphical way. This application offers a canvas to manipulate a bayesian network through jsAgrum. Their is to mode to use. The graphical mode allows to create and modify a bayesian network. The inference mode allows to express the evidence and sees the different values of each node.

## Modules

- Chart utilities regroups the functions that manage the inference charts.
- Graph Utilities contains the functions to manipulate the graph in both graphical and inference mode.
- Tables utilities regroups the functions that manage the potential table.
- Events utilities contains the functions that concerns the events and buttons.
- JsAgrum utilities regroups the functions to load and manage the jsAgrum object.
- Toolbar utilities contains all the functions to manage the toolbar.
